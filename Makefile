install:
	mvn validate && mvn compile && mvn package

run-win:
	java  -classpath libs\jxmaps-1.3.1.jar;libs\license.jar;libs\jxmaps-win.jar;target\fit-1.0-SNAPSHOT.jar

run-linux:
	java -classpath libs/jxmaps-1.3.1.jar:libs/license.jar:libs/jxmaps-linux32.jar:target/fit-1.0-SNAPSHOT.jar vutbr.fit.pdb17.App

run-linux64:
	java -classpath libs/jxmaps-1.3.1.jar:libs/license.jar:libs/jxmaps-linux64.jar:target/fit-1.0-SNAPSHOT.jar vutbr.fit.pdb17.App

all: run-linux
