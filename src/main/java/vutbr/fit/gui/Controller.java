package vutbr.fit.gui;

import vutbr.fit.configurator.Configurator;
import vutbr.fit.db.model.EventModel;
import vutbr.fit.db.model.PlaceModel;
import vutbr.fit.gui.model.*;

import java.io.File;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;

public class Controller {
    private Configurator configurator;

    private ArrayList<PlaceListModel> searchResults;
    private PlaceDetailModel loadedPlace;
    private ArrayList<EventListModel> placeEvents;
    private EventDetailModel loadedEvent;

    private ArrayList<PlaceListModel> userPlaces;
    private ArrayList<EventListModel> userEvents;
    private ArrayList<PlaceListModel> userFavourites;

    private UserModel user;
    public int idPlace;
    private CoordinatesModel startingPoint;

    private int radius;
    private String searchNamePlace = "";


    public Controller(Configurator configurator) {
        this.radius = -1;
        this.configurator = configurator;
        this.user = new UserModel();
        this.startingPoint = new CoordinatesModel();
        this.userPlaces = new ArrayList<>();
        this.userEvents = new ArrayList<>();
        resetSession();
    }

    public void resetSession() {
        this.loadedPlace = new PlaceDetailModel();
        this.searchResults = new ArrayList<>();
        this.placeEvents = new ArrayList<>();
        this.loadedEvent = new EventDetailModel();
        this.userFavourites = new ArrayList<>();
    }

    /**
     * Authenticate of user by username and password
     * @param username
     * @param password
     * @return Success of operation
     */
    public boolean authenticateUser(String username, String password) {
        vutbr.fit.db.model.UserModel model = (vutbr.fit.db.model.UserModel) configurator.getModel(vutbr.fit.db.model.Model.Names.USER);
        ResultSet user = model.authenticate(username, password);
        if (user == null) return false;
        try {
            this.user.setId(user.getInt(vutbr.fit.db.model.UserModel.COL_ID));
            this.user.setSurname(user.getString(vutbr.fit.db.model.UserModel.COL_SURNAME));
            this.user.setName(user.getString(vutbr.fit.db.model.UserModel.COL_NAME));
        } catch (SQLException e) {
            System.err.println(e);
        }
        return true;
    }

    /**
     *  Register new user
     * @param name
     * @param surname
     * @param email
     * @param password
     * @return Success of operation
     */
    public boolean registerUser(String name, String surname, String email, String password){
        vutbr.fit.db.model.UserModel model = (vutbr.fit.db.model.UserModel) configurator.getModel(vutbr.fit.db.model.Model.Names.USER);
        return model.registerUser(name, surname, email, password);
    }

    /**
     * Retrieves user's location and returns success of the operation
     *
     * @return Success of operation
     */
    public boolean loadUserPlaces() {
        userPlaces.clear();
        vutbr.fit.db.model.UserModel model = (vutbr.fit.db.model.UserModel) configurator.getModel(vutbr.fit.db.model.Model.Names.USER);
        try{
            ResultSet placeData = model.getUserPlaces(user.getId());
            PlaceListModel place;
            while(placeData.next()) {
                // empty location
                if (placeData.getObject(PlaceModel.COL_LOCATION) == null)
                    continue;
                place = new PlaceListModel(placeData.getInt(PlaceModel.COL_ID), placeData.getString(PlaceModel.COL_NAME));
                place.setCoordinates(PlaceModel.convertLocationToCoords(placeData.getObject(PlaceModel.COL_LOCATION)));
                place.setAddress(placeData.getString(PlaceModel.COL_ADDRESS));
                userPlaces.add(place);
            }
        }catch(SQLException e){
            System.err.println(e);
            return false;
        }
        return (userPlaces.size() != 0);
    }

    public boolean loadUserEvents() {
        vutbr.fit.db.model.UserModel model = (vutbr.fit.db.model.UserModel) configurator.getModel(vutbr.fit.db.model.Model.Names.USER);
        try{
            ResultSet rs = model.getUserEvents(user.getId());
            while(rs.next()){
                vutbr.fit.db.model.EventModel eventModel = (vutbr.fit.db.model.EventModel) configurator.getModel(vutbr.fit.db.model.Model.Names.EVENT);
                ResultSet eventData = eventModel.getEventData(rs.getInt("ID"));
                if(eventData.next()){
                    EventListModel event = new EventListModel(eventData.getInt("ID"), eventData.getString("NAME"), eventData.getString("DESCRIPTION"));

                    DateFormat df = new SimpleDateFormat("dd.MM.yyyy");

                    event.setStart(df.format(eventData.getDate("BEGIN")));
                    event.setEnd(df.format(eventData.getDate("END")));
                    event.setPlaceId(eventData.getInt("PLACE_ID"));
                    userEvents.add(event);
                }
            }
        }catch(SQLException e){
            return false;
        }
        if(userEvents.size() == 0)
            return false;
        return true;
    }

    /**
     * Search place by name or by name and location
     *
     * @param placeName
     */
    public void searchByName(String placeName) {
        searchNamePlace = placeName;
        vutbr.fit.db.model.PlaceModel place = (vutbr.fit.db.model.PlaceModel) configurator.getModel(vutbr.fit.db.model.Model.Names.PLACE);
        if (radius == -1)
            searchResults = place.getPlacesByName(placeName, null, false);
        else
            searchResults = place.getPlacesByNameAndPosition(placeName, startingPoint, radius, false);
    }

    public void searchByImage(String startingAddress, File upload) {
        // todo query: hladanie podla obrazku
    }

    public void loadEvent(EventListModel eventListModel) {
        vutbr.fit.db.model.EventModel event = (vutbr.fit.db.model.EventModel) configurator.getModel(vutbr.fit.db.model.Model.Names.EVENT);
        this.loadedEvent = event.loadEventDetail(eventListModel.getId());
        // todo query: load EventDetailModel + save to loadedEvent
    }

    /**
     * Load place, comments and photos
     *
     * @param detail Place to load
     */
    public void loadPlace(PlaceListModel detail) {
        vutbr.fit.db.model.PlaceModel place = (vutbr.fit.db.model.PlaceModel) configurator.getModel(vutbr.fit.db.model.Model.Names.PLACE);
        loadedPlace = place.loadPlaceDetail(detail.getId());
    }

    /**
     * Load favorites data of current user
     */
    public void loadUserFavourites() {
        PlaceModel model = (PlaceModel) configurator.getModel(vutbr.fit.db.model.Model.Names.PLACE);
        userFavourites = model.selectFavoritePlaces(user.getId());
    }

    /**
     * Delete place
     *
     * @param model Place to delete
     */
    public void removePlace(PlaceListModel model) {
        vutbr.fit.db.model.PlaceModel place = (vutbr.fit.db.model.PlaceModel) configurator.getModel(vutbr.fit.db.model.Model.Names.PLACE);
        place.delete(model.getId());
    }

    public void removeEvent(EventListModel model) {
        vutbr.fit.db.model.EventModel event = (vutbr.fit.db.model.EventModel) configurator.getModel(vutbr.fit.db.model.Model.Names.EVENT);
        event.delete(model.getId());
    }

    public void removeImage(ImageModel photo) {
        // todo image
    }
    /**
     * Create or update place
     *
     * @param newPlace
     * @return Success operation
     */
    public boolean addOrUpdatePlace(PlaceDetailModel newPlace) {
        vutbr.fit.db.model.PlaceModel model = (vutbr.fit.db.model.PlaceModel) configurator.getModel(vutbr.fit.db.model.Model.Names.PLACE);
        if (newPlace.getId() != 0)
            return model.update(newPlace.getId(), newPlace.getPlaceName(), newPlace.getAddress(), newPlace.getDescription(), newPlace.getCoordinates());
        else
            model.create(newPlace.getPlaceName(), newPlace.getAddress(), newPlace.getDescription(), newPlace.getCoordinates(), user.getId());
        return true;
    }

    /**
     * Create or update event
     *
     * @param detail
     * @return Success of operation
     */
    public boolean addOrUpdateEvent(EventListModel detail) {
        vutbr.fit.db.model.EventModel model = (vutbr.fit.db.model.EventModel) configurator.getModel(vutbr.fit.db.model.Model.Names.EVENT);
        if (detail.getId() != 0)
            return model.update(detail.getId(), detail.getTitle(), detail.getText(), detail.getStart(), detail.getEnd());
        else
            return model.create(detail.getTitle(), detail.getText(), detail.getStart(), detail.getEnd(), idPlace, user.getId());
    }

    public boolean addOrUpdatePhoto(ImageModel imageModel) {
        vutbr.fit.db.model.ImageModel image = (vutbr.fit.db.model.ImageModel) configurator.getModel(vutbr.fit.db.model.Model.Names.IMAGE);
        return image.add(user.getId(), getLoadedPlace().getId(), imageModel.image, configurator);
    }

    public boolean addPlaceComment(CommentModel comment){
        vutbr.fit.db.model.PlaceModel place = (vutbr.fit.db.model.PlaceModel) configurator.getModel(vutbr.fit.db.model.Model.Names.PLACE);
        return place.addComment(comment, getLoadedPlace().getId(), user.getId());
    }

    public boolean addEventComment(CommentModel comment) {
        vutbr.fit.db.model.EventModel event = (vutbr.fit.db.model.EventModel) configurator.getModel(vutbr.fit.db.model.Model.Names.EVENT);
        return event.addComment(comment, getLoadedEvent().getId(), user.getId());
    }

    /**
     * Remove or add place to favorite list
     */
    public void toggleFavourite() {
        PlaceModel model = (PlaceModel) configurator.getModel(vutbr.fit.db.model.Model.Names.PLACE);
        if (model.isFavoritePlace(user.getId(), loadedPlace.getId())) {
            model.removeFavorite(loadedPlace.getId(), user.getId());
        } else {
            model.addFavorite(loadedPlace.getId(), user.getId());
        }
    }
    /**
     * Chcech if place is favorite
     * @return
     */
    public boolean isPlaceFavourite() {
        PlaceModel model = (PlaceModel) configurator.getModel(vutbr.fit.db.model.Model.Names.PLACE);
        return model.isFavoritePlace(user.getId(), loadedPlace.getId());
    }

    /**
     * Set the place filter by radius
     *
     * @param radius Radius in kilometers
     */
    public void filterByRadius(int radius) {
        this.radius = radius;
    }

    /**
     * Sorting places by distance from point
     *
     * @param asc Type of sorting
     */
    public void orderResultsAlph(boolean asc) {
        Collections.sort(this.searchResults, (o1, o2) -> o1.getPlaceName().compareToIgnoreCase(o2.getPlaceName()));
        if(!asc)
            Collections.reverse(this.searchResults);
    }

    /**
     * Sorting places by distance from point
     *
     * @param asc Type of sorting
     */
    public void orderResultsDist(boolean asc) {
        vutbr.fit.db.model.PlaceModel place = (vutbr.fit.db.model.PlaceModel) configurator.getModel(vutbr.fit.db.model.Model.Names.PLACE);
        if (radius == -1)
            searchResults = place.getPlacesByName(searchNamePlace, startingPoint, true);
        else
            searchResults = place.getPlacesByNameAndPosition(searchNamePlace, startingPoint, radius, true);
        if (!asc)
            Collections.reverse(this.searchResults);
    }

    public ArrayList<CommentModel> getPlaceComments() {
        return loadedPlace.getComments();
    }

    public ArrayList<CommentModel> getEventComments() {
        return loadedEvent.getComments();
    }

    public PlaceDetailModel getLoadedPlace() {
        return loadedPlace;
    }

    public EventDetailModel getLoadedEvent() {
        return loadedEvent;
    }

    public boolean testPhotoOwnership(ImageModel currentPhoto) {
//        return this.user.getId() == currentPhoto.getUserId();
        return true;
    }

    public void setUser(String user) {
        this.user.setUsername(user);
    }

    public void setStartingPoint(CoordinatesModel startingPoint) {
        this.startingPoint = startingPoint;
    }

    public ArrayList<PlaceListModel> getSearchResults() {
        return searchResults;
    }

    public ArrayList<EventListModel> getEventList() {
        EventModel model = (EventModel) configurator.getModel(vutbr.fit.db.model.Model.Names.EVENT);
        placeEvents = model.getEventsByPlace(loadedPlace.getId());
        System.out.println(loadedPlace.getId());
        return placeEvents;
    }

    public boolean isOwnerOfPlace() {
        PlaceModel model = (PlaceModel) configurator.getModel(vutbr.fit.db.model.Model.Names.PLACE);
        ResultSet result = model.getPlaceData(loadedPlace.getId());
        try {
            return result != null && result.getInt(PlaceModel.COL_USER_ID) == user.getId();
        } catch (SQLException e) {
            return false;
        }
     }
    public boolean isOwnerOfEvent() {
        EventModel model = (EventModel) configurator.getModel(vutbr.fit.db.model.Model.Names.EVENT);
        ResultSet result = model.getEventData(loadedEvent.getId());
        try {
            return result != null && result.getInt(EventModel.COL_USER_ID) == user.getId();
        } catch (SQLException e) {
            return false;
        }
     }

    public ArrayList<EventListModel> getUserEvents() {
        return userEvents;
    }

    public ArrayList<PlaceListModel> getUserPlaces() {
        return userPlaces;
    }

    public void setUsername(String username) {
        this.user.setUsername(username);
    }

    public String getUsername() {
        return this.user.getUsername();
    }

    public CoordinatesModel getStartingPoint() {
        return this.startingPoint;
    }

    public int getRadius() {
        return radius;
    }

    public ArrayList<PlaceListModel> getUserFavourites() {
        return this.userFavourites;
    }

    public void resetPlace() {
        this.loadedPlace = new PlaceDetailModel();
    }

    public void resetEvent() {
        this.loadedEvent = new EventDetailModel();
    }

    public void setRadius(int radius) {
        this.radius = radius;
    }
}
