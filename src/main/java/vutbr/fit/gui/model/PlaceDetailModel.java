package vutbr.fit.gui.model;

import java.util.ArrayList;

/**
 * PlaceDetailModel contains additional information that is only visible on the place detail page. It contains all
 * comments, favourites, and images of the instance.
 */
public class PlaceDetailModel extends PlaceListModel {
    private String placeDescription;
    private ArrayList<ImageModel> images;
    private ArrayList<CommentModel> comments;
    private ArrayList<FavoriteModel> favorites;

    public PlaceDetailModel(PlaceListModel model) {
        super(model);
    }

    /**
     * Default constructor, initializes all values to empty/0.
     */
    public PlaceDetailModel() {
        super();
        this.placeDescription = "";
        this.comments = new ArrayList<>();
        this.images = new ArrayList<>();
        this.favorites = new ArrayList<>();
    }


    /**
     * Comment getter
     * @return User comments
     */
    public ArrayList<CommentModel> getComments() {
        return this.comments;
    }

    /**
     * Comment setter
     * @param comments User comments
     */
    public void setComments(ArrayList<CommentModel> comments) {
        this.comments = comments;
    }

    /**
     * Description setter
     * @param description Place description
     */
    public void setDescription(String description) {
        this.placeDescription = description;
    }

    /**
     * Description getter
     * @return Place description
     */
    public String getDescription() {
        return placeDescription;
    }

    /**
     * Images setter
     * @param images Place images
     */
    public void setImages(ArrayList<ImageModel> images) {
        this.images = images;
    }

    /**
     * Images getter
     * @return Place images
     */
    public ArrayList<ImageModel> getImages() {
        return images;
    }

    /**
     * Favourites setter
     * @param favorites User's favourite places array list
     */
    public void setFavorites(ArrayList<FavoriteModel> favorites) { this.favorites = favorites; }

    /**
     * Favourites getter
     * @return User's favourites
     */
    public ArrayList<FavoriteModel> getFavorites(){ return favorites; }
}
