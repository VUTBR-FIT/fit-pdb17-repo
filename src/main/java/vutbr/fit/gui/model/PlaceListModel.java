package vutbr.fit.gui.model;

import java.util.ArrayList;

/**
 * PlaceListModel is the class representing data shown in items of place lists. Each such item contains the place name,
 * address, and a list of coordinates.
 */
public class PlaceListModel extends Model{
    private String placeName;
    private String address;
    private ArrayList<CoordinatesModel> points;

    /**
     * Default constructor, used to create a new place. Initialized all values to empty.
     */
    public PlaceListModel() {
        this.id = 0;
        this.points = new ArrayList<>();
        this.placeName = this.address = "";
    }

    /**
     * Constructor used to load an existing place from the database.
     * @param id Place ID
     * @param placeName Place name
     */
    public PlaceListModel(int id, String placeName) {
        this();
        this.id = id;
        this.placeName = placeName;
    }

    /**
     * Constructor used to create a new place with a set name
     * @param placeName Place name
     */
    public PlaceListModel(String placeName) {
        this();
        this.placeName = placeName;
    }

    /**
     * Copy an existing instance.
     * @param model Existing instance
     */
    public PlaceListModel(PlaceListModel model) {
        this.id = model.getId();
        this.placeName = model.getPlaceName();
        this.address = model.address;
    }

    /**
     * Address getter.
     * @return Place address
     */
    public String getAddress() {
        return address;
    }

    /**
     * Address setter.
     * @param addressLine1 Places address
     */
    public void setAddress(String addressLine1) {
        this.address = addressLine1;
    }

    /**
     * Place name getter.
     * @return Place name
     */
    public String getPlaceName() {
        return placeName;
    }

    /**
     * Place name setter
     * @param placeName Place name
     */
    public void setPlaceName(String placeName) {
        this.placeName = placeName;
    }

    /**
     * Place vertices getter.
     * @return Place vertices
     */
    public ArrayList<CoordinatesModel> getCoordinates() {
        return this.points;
    }

    /**
     * Place vertices setter.
     * @param coordinates Place vertices
     */
    public void setCoordinates(ArrayList<CoordinatesModel> coordinates) {
        this.points = coordinates;
    }
}

