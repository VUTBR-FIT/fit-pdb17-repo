package vutbr.fit.gui.model;

/**
 * CoordinatesModel class represent geographical coordinates of a place.
 */
public class CoordinatesModel extends Model {
    public float x;
    public float y;

    /**
     * Constructor that initializes the instance to the provided longitude/latitude values.
     * @param x Longitude value
     * @param y Latitude value
     */
    public CoordinatesModel(float x, float y) {
        this.x = x;
        this.y = y;
    }

    /**
     * Default constructor that initialized the instance to the equator coordinates.
     */
    public CoordinatesModel() {
        this.x = 0;
        this.y = 0;
    }

    @Override
    public String toString() {
        return x + "° " + y + "'";
    }
}
