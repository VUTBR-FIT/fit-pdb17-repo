package vutbr.fit.gui.model;

/**
 * Model is the abstract base class for all objects containing interpreted data loaded from the database.
 */
abstract class Model {
    protected int id;

    /**
     * Model ID getter.
     * @return Model Id
     */
    public int getId() {
        return this.id;
    }

    /**
     * Model ID setter.
     * @param id Model Id
     */
    public void setId(int id) {
        this.id = id;
    }
}
