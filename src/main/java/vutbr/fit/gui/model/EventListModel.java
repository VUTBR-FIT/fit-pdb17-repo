package vutbr.fit.gui.model;

/**
 * EventListModel is the class representing data shown in items of event lists. Each such item contains the event name,
 * location, duration, and is tied to an existing location.
 */
public class EventListModel extends Model {
    private String start;
    private String end;
    private String title;
    private String text;
    private int placeId;

    /**
     * Default constructor, sets all values to empty or 0.
     */
    EventListModel() {
        this.id = this.placeId = 0;
        this.start = this.end = this.title = this.text = "";
    }

    /**
     * Constructor used in the case of loading an event from the database that sets all values.
     * @param id Event Id
     * @param title Event title
     * @param text Event description
     */
    public EventListModel(int id, String title, String text) {
        this.id = id;
        this.text = text;
        this.title = title;
        this.start = "1. 1. 2017";
        this.end = "31. 12. 2017";
    }

    /**
     * Set Id of the place this event is tied to. This value is used the event location on map and/or plan the event.
     * @param placeId Parent place Id
     */
    public void setPlaceId(int placeId) {
        this.placeId = placeId;
    }

    /**
     * Parent place identifier getter.
     * @return
     */
    public int getPlaceId() {
        return placeId;
    }

    public String getDuration() {
        return this.start + " to " + this.end;
    }

    public String getStart() {
        return start;
    }

    public void setStart(String start) {
        this.start = start;
    }

    public String getEnd() {
        return end;
    }

    public void setEnd(String end) {
        this.end = end;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}

