package vutbr.fit.gui.model;

import java.awt.image.BufferedImage;

/**
 * ImageModel class represents the images displayed at location/event detail pages. Each picture is tied to a user that
 * uploaded it and therefore can edit and/or delete it.
 */
public class ImageModel extends Model {
    private int userId;
    public BufferedImage image; // todo asi iny datatype?

    /**
     * Constructor used for new image uploads.
     * @param image Image blob
     */
    public ImageModel(BufferedImage image) {
        this.id = 0;
        this.image = image;
    }

    /**
     * Constructor used to load images from the database.
     * @param id Image Id
     * @param image Image blob
     */
    public ImageModel(int id, BufferedImage image) {
        this(image);
        this.id = id;
    }

    /**
     * Owner ID getter
     * @return Id of owning user
     */
    public int getUserId() {
        return userId;
    }

    /**
     * Owner ID setter
     * @param userId Id of owning user
     */
    public void setUserId(int userId) {
        this.userId = userId;
    }

    /**
     * Loaded image getter
     * @return Image
     */
    public BufferedImage getImage() {
        return this.image;
    }

    /**
     * Loaded image setter
     * @param image Image
     */
    public void setImage(BufferedImage image) {
        this.image = image;
    }
}
