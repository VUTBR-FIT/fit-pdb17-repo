package vutbr.fit.gui.model;

public class FavoriteModel extends Model {
    private int userId;
    private String username;

    public FavoriteModel(){
        this.id = 0;
    }

    public FavoriteModel(int id, String username){
        this();
        this.userId = id;
        this.username = username;
    }

    public void setUserId(int id){ this.userId = id; }

    public int getUserId(){ return this.userId; }

    public void setUsername(String username){ this.username = username; }

    public String getUsername(){ return this.username; }
}
