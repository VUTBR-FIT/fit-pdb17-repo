package vutbr.fit.gui.model;

/**
 * UserModel class represents a user that can log in into system, add/edit places and events, and leave comments, and
 * photos.
 */
public class UserModel extends Model {
    /**
     * Login name of user
     */
    private String username;
    /**
     * First name of user
     */
    private String name;
    /**
     * Last name of user
     */
    private String surname;

    /**
     * Default constructor, initializes all values to empty.
     */
    public UserModel() {
        this.id = 0;
        this.username = "";
    }

    /**
     * Username getter.
     * @return Username
     */
    public String getUsername() {
        return username;
    }

    /**
     * Username setter.
     * @param username Username
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     * ID setter.
     * @param id User Id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * ID getter.
     * @return User Id
     */
    public String getName() {
        return name;
    }

    /**
     * Name setter.
     * @param name First name of user
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Surname getter.
     * @return User surname
     */
    public String getSurname() {
        return surname;
    }

    /**
     * Surname setter.
     * @param surname User surname
     */
    public void setSurname(String surname) {
        this.surname = surname;
    }
}
