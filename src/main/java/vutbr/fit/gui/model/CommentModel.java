package vutbr.fit.gui.model;

/**
 * CommentModel is the class representing comments loaded to and from the database. It contains all displayed information
 * about a comment: author username, and comment title and body.
 */
public class CommentModel extends Model{
    /**
     * Displayed comment parameters
     */
    private String title;
    private String username;
    private String messageText;

    /**
     * Constructor used to create a new comment; Id is set to 0, which signalizes a new instance.
     * @param username Displayed username
     * @param title Comment title
     * @param messageText Comment body
     */
    public CommentModel(String username, String title, String messageText) {
        this.id = 0;
        this.username = username;
        this.title = title;
        this.messageText = messageText;
    }

    /**
     * Constructor used when loading an existing comment from the database.
     * @param id Comment ID
     * @param username Displayed username
     * @param title Comment title
     * @param messageText Comment body
     */
    public CommentModel(int id, String username, String title, String messageText) {
        this(username, title, messageText);
        this.id = id;
    }

    /**
     * Message body getter.
     * @return Comment body
     */
    public String getMessageText() {
        return messageText;
    }

    /**
     * Message body getter.
     * @param messageText Comment body
     */
    public void setMessageText(String messageText) {
        this.messageText = messageText;
    }

    /**
     * Username getter.
     * @return Displayed username
     */
    public String getUsername() {
        return username;
    }

    /**
     * Username setter.
     * @param username Displayed username.
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     * Title getter
     * @return Comment title
     */
    public String getTitle() {
        return title;
    }

    /**
     * Title setter
     * @param title Comment title
     */
    public void setTitle(String title) {
        this.title = title;
    }
}
