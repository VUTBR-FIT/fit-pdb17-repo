package vutbr.fit.gui.model;

import java.util.ArrayList;

/**
 * EventDetailModel contains additional information that is only visible on the event detail page. It contains all
 * comments and images of the instance.
 */
public class EventDetailModel extends EventListModel {
    private ArrayList<CommentModel> comments;
    private ArrayList<ImageModel> images;

    /**
     * Default constructor, initializes the model to all empty values.
     */
    public EventDetailModel() {
        super();
        this.comments = new ArrayList<>();
        this.images = new ArrayList<>();
    }

    /**
     * Comments getter.
     * @return Event comments
     */
    public ArrayList<CommentModel> getComments() {
        return this.comments;
    }

    /**
     * Comments setter
     * @param comments Event comments
     */
    public void setComments(ArrayList<CommentModel> comments) {
        this.comments = comments;
    }

    /**
     * Images getter
     * @return Event images
     */
    public ArrayList<ImageModel> getImages() {
        return images;
    }

    /**
     * Images setter
     * @param images Event images
     */
    public void setImages(ArrayList<ImageModel> images) {
        this.images = images;
    }
}
