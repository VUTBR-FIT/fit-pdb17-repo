package vutbr.fit.gui.view;

import vutbr.fit.configurator.Configurator;
import vutbr.fit.gui.Controller;
import vutbr.fit.gui.view.misc.LoginDialog;
import vutbr.fit.gui.view.misc.RegisterDialog;

import javax.swing.*;

import static vutbr.fit.gui.view.Page.SEARCH_PAGE;

/**
 * MapApplication class is responsible for the run of the application and management of its resources.
 */
public class MapApplication extends JFrame {
    private Controller controller;
    private UserMenubar menubar;
    private ContentLayout layout;
    public Page currentPage;

    public MapApplication(Configurator configurator) {
        this.controller = new Controller(configurator);
        this.controller.setUsername("");
        this.currentPage = SEARCH_PAGE;

        this.setTitle("Places Data Base");
        this.setSize(1200, 800);
        this.setLocationRelativeTo(this);
        this.setResizable(false);
        this.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

        this.menubar = new UserMenubar(this);
        this.setJMenuBar(this.menubar);

        updateFrame(SEARCH_PAGE);
    }

    public void createLoginDialog() {
        LoginDialog login = new LoginDialog(this);
        login.setVisible(true);
    }

    public void setUsername(String username) {
        this.controller.setUser(username);
        menubar.updateMenubar(this.currentPage);
    }

    public String getUsername() {
        return this.controller.getUsername();
    }

    public boolean authenticateUser(String text, char[] password) {
        if (this.controller.authenticateUser(text, new String(password))) {
            setUsername(text);
            return true;
        }
        return false;
    }

    private void updateLayout(Page pageType) {
        this.getContentPane().removeAll();
        layout = new ContentLayout(this, pageType);
        this.getContentPane().add(layout);
    }

    /**
     * Update the content pane based on the current page type
     * @param pageType The type of requested screen
     */
    public void updateFrame(Page pageType) {
        this.currentPage = pageType;
        this.menubar.updateMenubar(pageType);
        updateLayout(pageType);
    }

    public Controller getController() {
        return controller;
    }

    public ContentLayout getContentLayout() {
        return this.layout;
    }

    public void createRegisterDialog() {
        RegisterDialog dialog = new RegisterDialog(this);
        dialog.setVisible(true);
    }
}
