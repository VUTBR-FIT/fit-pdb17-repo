package vutbr.fit.gui.view;

import vutbr.fit.gui.model.CommentModel;
import vutbr.fit.gui.model.ImageModel;
import vutbr.fit.gui.model.PlaceListModel;
import vutbr.fit.gui.view.menu.SideMenuLabel;
import vutbr.fit.gui.view.misc.ImageUploadPanel;
import vutbr.fit.gui.view.misc.TransparentButton;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;

class UserMenubar extends JMenuBar {
    private MapApplication parent;
    private JButton search;
    private JComponent usernameMenu;

    // initial constructor
    UserMenubar(MapApplication parent) {
        this.parent = parent;
        updateMenubar(Page.SEARCH_PAGE);
    }

    void updateMenubar(Page pageType) {
        this.removeAll();
        generateUserMenu();
        this.add(Box.createRigidArea(new Dimension(10,0)));
        generateOptionMenu(pageType);
        this.add(Box.createHorizontalGlue());
        if (pageType != Page.SEARCH_PAGE)
            addSearchButton();
        this.updateUI();
        parent.revalidate();
    }

    private void generateOptionMenu(Page pageType) {
        switch (pageType) {
            case SEARCH_PAGE:
                if (!"".equals(parent.getController().getUsername()))
                    generateAddPlaceButton();
                break;
            case SEARCH_RESULTS:
                generateSearchResultMenu();
                break;
            case DETAIL_PLACE:
                if (!"".equals(parent.getController().getUsername()))
                    generateDetailMenu(true);
                break;
            case DETAIL_EVENT:
                if (!"".equals(parent.getController().getUsername()))
                    generateDetailMenu(false);
                break;
            case EDIT_PLACE:
            case EDIT_EVENT:
                // no params
                break;
            case USER_PLACES:
                if (!"".equals(parent.getController().getUsername()))
                    generateAddPlaceButton();
                break;
            case USER_EVENTS:
                // no params
                break;
        }
    }

    private void generateAddPlaceButton() {
        if ("".equals(parent.getController().getUsername()))
            return;
        JButton addPlace = new TransparentButton("Add location");
        addPlace.addActionListener(e -> {
            parent.getController().resetPlace();
            parent.updateFrame(Page.EDIT_PLACE);
        });
        this.add(addPlace);
    }

    private void generateDetailMenu(final boolean isLocation) {
        generateCommentButton();
        generateEventButton(isLocation);
        generateEditButton(isLocation);
        generatePhotoButton();
    }

    private void generateSearchResultMenu() {
        generateOrderMenu();
    }

    private void generateOrderMenu() {
        JMenu menu = new JMenu("Order by");
        JMenuItem item = new JMenuItem("A - Z");

        item.addActionListener(e -> new SearchMenubar(parent).filterResultsAlphabetically(true));
        menu.add(item);
        item = new JMenuItem("Z - A");

        item.addActionListener(e -> new SearchMenubar(parent).filterResultsAlphabetically(false));
        menu.add(item);

        item = new JMenuItem("Closest");
        item.addActionListener(e -> new SearchMenubar(parent).filterResultsByDistance(true));
        menu.add(item);

        item = new JMenuItem("Furthest");
        item.addActionListener(e -> new SearchMenubar(parent).filterResultsByDistance(false));
        menu.add(item);
        this.add(menu);
    }

    private void generateUserMenu() {
        if ("".equals(parent.getUsername()))
            generateAnonUserMenu();
        else
            generateLoggedUserMenu();
    }

    private void generateLoggedUserMenu() {
        usernameMenu = new JMenu(parent.getUsername());
        JMenu menu = (JMenu) usernameMenu;

        JMenuItem item = new JMenuItem(new AbstractAction("My places") {
            @Override
            public void actionPerformed(ActionEvent e) {
                parent.getController().loadUserPlaces();
                parent.updateFrame(Page.USER_PLACES);
            }
        });
        menu.add(item);

        item = new JMenuItem(new AbstractAction("My events") {
            @Override
            public void actionPerformed(ActionEvent e) {
                parent.getController().loadUserEvents();
                parent.updateFrame(Page.USER_EVENTS);
            }
        });
        menu.add(item);

        item = new JMenuItem(new AbstractAction("My favourites") {
            @Override
            public void actionPerformed(ActionEvent e) {
                parent.getController().loadUserFavourites();
                parent.updateFrame(Page.USER_FAVOURITES);
            }
        });
        menu.add(item);
        menu.addSeparator();

        item = new JMenuItem(new AbstractAction("Log out") {
            @Override
            public void actionPerformed(ActionEvent e) {
                parent.setUsername("");
                parent.getController().resetSession();
                parent.updateFrame(Page.SEARCH_PAGE);
            }
        });
        menu.add(item);
        this.add(usernameMenu);
    }

    private void generateAnonUserMenu() {
        usernameMenu = new TransparentButton("Log in");
        JButton temp = (JButton) usernameMenu;
        temp.addActionListener(e -> parent.createLoginDialog());
        this.add(usernameMenu);

        usernameMenu = new TransparentButton("Sign up");
        temp = (JButton) usernameMenu;
        temp.addActionListener(e -> parent.createRegisterDialog());
        this.add(usernameMenu);
    }

    private void addSearchButton() {
        search = new TransparentButton("Search");
        search.addActionListener(e -> {
            parent.getController().resetSession();
            parent.updateFrame(Page.SEARCH_PAGE);
        });
        this.add(search);
    }

    private void generateCommentButton() {
        JButton button = new TransparentButton("Add comment");
        button.addActionListener(e -> {
            JTextField commentTitle = new JTextField(20);
            JTextArea commentText = new JTextArea(5, 20);
            commentText.setLineWrap(true);
            final JComponent[] inputs = new JComponent[] {
                    new SideMenuLabel("Title"),
                    commentTitle,
                    new SideMenuLabel("Comment"),
                    new JScrollPane(commentText)
            };
            int result = JOptionPane.showConfirmDialog(null, inputs, "Add comment", JOptionPane.PLAIN_MESSAGE);
            if (result == JOptionPane.OK_OPTION) {
                CommentModel comment = new CommentModel(parent.getController().getUsername(), commentTitle.getText(), commentText.getText());
                if (parent.currentPage == Page.DETAIL_PLACE) {
                    parent.getController().addPlaceComment(comment);
                    parent.updateFrame(parent.currentPage);
                } else if (parent.currentPage == Page.DETAIL_EVENT) {
                    parent.getController().addEventComment(comment);
                    parent.updateFrame(parent.currentPage);
                }
            }
        });
    }

    private void generateEditButton(final boolean isLocation) {
        JButton button = new TransparentButton("Edit " + (isLocation ? "location" : "event"));
        button.addActionListener(e -> {
            if (isLocation)
                parent.updateFrame(Page.EDIT_PLACE);
            else
                parent.updateFrame(Page.EDIT_EVENT);
        });
        this.add(button);
    }

    private void generatePhotoButton() {
        JButton button = new TransparentButton("Add photo");
        button.addActionListener(e -> {
            ImageUploadPanel panel = new ImageUploadPanel();
            final JComponent[] inputs = new JComponent[] { panel };
            int result = JOptionPane.showConfirmDialog(parent, inputs, "New photo", JOptionPane.OK_CANCEL_OPTION);
            if (result == JOptionPane.YES_OPTION)
                parent.getController().addOrUpdatePhoto(new ImageModel(panel.getCropPanel().getCroppedImage()));
        });
        this.add(button);
    }

    private void generateEventButton(boolean isLocation) {
        if (isLocation) {
            JButton button = new TransparentButton("Add event");
            button.addActionListener(e -> {
                PlaceListModel model = new PlaceListModel(parent.getController().getLoadedPlace());
                parent.getController().resetEvent();
                parent.getController().getLoadedEvent().setPlaceId(model.getId());
                parent.updateFrame(Page.EDIT_EVENT);
            });
            this.add(button);
        }
    }

    /**
     * SearchMenubar serves as a hub for useful methods that do not fit elsewhere.
     */
    public static class SearchMenubar {
        private MapApplication parent;

        SearchMenubar(MapApplication parent) {
            this.parent = parent;
        }

        void filterResultsAlphabetically(boolean asc) {
            this.parent.getController().orderResultsAlph(asc);
            this.parent.updateFrame(this.parent.currentPage);
        }

        void filterResultsByDistance(boolean asc) {
            this.parent.getController().orderResultsDist(asc);
            this.parent.updateFrame(this.parent.currentPage);
        }
    }
}
