package vutbr.fit.gui.view.menu;

import vutbr.fit.gui.model.EventListModel;
import vutbr.fit.gui.view.MapApplication;
import vutbr.fit.gui.view.Page;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;

/**
 * UserEventsMenu class represents the side menu containing the list of events a user has access to.
 */
public class UserEventsMenu extends SideMenu {
    public UserEventsMenu(MapApplication parent) {
        super(parent);
        SideMenuLabel header = new SideMenuLabel("My events");
        this.content.add(header);
        generateEvents();
    }

    private void generateEvents() {
        int listHeight = 0;
        for (EventListModel model: parent.getController().getUserEvents())
            listHeight += generateEventListItem(model);
        this.content.setPreferredSize(new Dimension(250, listHeight + 150));
    }

    private int generateEventListItem(EventListModel model) {
        JPanel panel = new JPanel(new GridBagLayout());

        GridBagConstraints gbc = new GridBagConstraints();
        gbc.gridx = 0;
        gbc.gridy = 0;

        JLabel title = new SearchResultLabel(model.getTitle());
        panel.add(title, gbc);

        JLabel text = new LongLabel(model.getText());
        gbc.gridy = 1;
        panel.add(text, gbc);

        JPanel duration = new JPanel(new FlowLayout());
        duration.setAlignmentX(LEFT_ALIGNMENT);
        JLabel label = new JLabel("Duration: ");
        duration.add(label);
        label = new JLabel(model.getDuration());
        label.setFont(new Font(label.getFont().getFontName(), Font.ITALIC, label.getFont().getSize()));
        duration.add(label);
        gbc.gridy = 2;
        panel.add(duration, gbc);

        JButton details = new JButton("Edit »");
        details.addActionListener(e -> {
            parent.getController().loadEvent(model);
            parent.updateFrame(Page.EDIT_EVENT);
        });
        gbc.gridy = 3;
        panel.add(details, gbc);

        panel.setBorder(new EmptyBorder(5, 10, 5, 5));
        this.content.add(panel);
        return panel.getPreferredSize().height;
    }
}
