package vutbr.fit.gui.view.menu;

import vutbr.fit.gui.model.PlaceListModel;
import vutbr.fit.gui.view.MapApplication;
import vutbr.fit.gui.view.Page;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;

/**
 * SideMenu is the abstract base class for all content side menus. It is located on the WEST border of the content pane.
 */
public abstract class SideMenu extends JScrollPane {
    JPanel content;
    MapApplication parent;

    SideMenu(MapApplication parent) {
        super();
        this.parent = parent;
        this.content = new JPanel();
        this.setViewportView(this.content);
        setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
        setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED);
        this.content.setPreferredSize(new Dimension(250, parent.getContentPane().getHeight() - 5));
    }

    int generatePlaceListItem(final PlaceListModel detail) {
        JPanel result = new JPanel(new GridBagLayout());
        GridBagConstraints gbc = new GridBagConstraints();
        gbc.gridx = 0;
        gbc.gridy = 0;

        result.setBorder(new EmptyBorder(0, 0, 10, 0));
        SearchResultLabel header = new SearchResultLabel(detail.getPlaceName());
        result.add(header, gbc);

        if (!"".equals(detail.getAddress())) {
            JLabel addressLine1 = new AddressLabel(detail.getAddress());
            addressLine1.setBorder(new EmptyBorder(0, 5, 0, 5));
            gbc.gridy = 1;
            result.add(addressLine1, gbc);
        }

        JLabel coordinates = new JLabel(detail.getCoordinates().toString());
        coordinates.setFont(new Font("Tahoma", Font.ITALIC, 12));
        coordinates.setAlignmentX(LEFT_ALIGNMENT);
        coordinates.setBorder(new EmptyBorder(0, 5, 10, 0));
        coordinates.setPreferredSize(new Dimension(220, 20));
        gbc.gridy = 3;
        result.add(coordinates, gbc);

        JButton seeDetails = new JButton("See details »");
        seeDetails.setAlignmentX(LEFT_ALIGNMENT);
        seeDetails.setPreferredSize(new Dimension(190, 25));
        seeDetails.addActionListener(e -> {
            parent.getController().loadPlace(detail);
            parent.updateFrame(Page.DETAIL_PLACE);
        });
        gbc.gridy = 4;
        gbc.gridwidth = 2;
        result.add(seeDetails, gbc);
        this.content.add(result);
        return result.getPreferredSize().height;
    }
}