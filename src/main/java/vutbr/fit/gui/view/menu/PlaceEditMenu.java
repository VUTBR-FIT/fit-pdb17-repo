package vutbr.fit.gui.view.menu;

import vutbr.fit.gui.model.PlaceDetailModel;
import vutbr.fit.gui.model.PlaceListModel;
import vutbr.fit.gui.view.MapApplication;
import vutbr.fit.gui.view.Page;
import vutbr.fit.gui.view.map.PlaceEditMap;

import javax.swing.*;
import java.awt.*;

/**
 * PlaceEditMenu class represents the side menu containing the form used to add or edit places.
 */
public class PlaceEditMenu extends SideMenu {
    private PlaceListModel detail;
    private JTextField placeName;
    private JTextArea placeDescription;
    private JTextField placeAddress;
    private JTextField coordinates_lat;
    private JTextField coordinates_lng;
    
    PlaceEditMenu(MapApplication parent) {
        super(parent);
    }
    
    public PlaceEditMenu(final MapApplication parent, final PlaceDetailModel detail) {
        super(parent);
        this.detail = detail;

        JLabel header = new SideMenuLabel((detail.getId() > 0 ? "Edit" : "Add") + " location");
        this.content.add(header);
        
        JLabel placeNameLabel = new SideMenuLabel("Location name");
        this.content.add(placeNameLabel);
        
        placeName = new JTextField(20);
        placeName.setMargin(new Insets(2, 5, 2, 5));
        placeName.setText(parent.getController().getLoadedPlace().getPlaceName());
        this.content.add(placeName);

        generateCoordinates();
        generatePlaceDescription();

        if (detail.getId() > 0) {
            JButton remove = new JButton("Remove location");
            remove.addActionListener(e -> {
                int response = JOptionPane.showConfirmDialog(parent,
                        "Are you sure you want to delete this location?",
                        "Delete location?",
                        JOptionPane.YES_NO_OPTION);
                if (response == JOptionPane.YES_OPTION) {
                    parent.getController().removePlace(detail);
                    JOptionPane.showConfirmDialog(parent, "Location successfully deleted.", "Location deleted",
                            JOptionPane.INFORMATION_MESSAGE);
                    parent.getController().resetPlace();
                    parent.updateFrame(Page.EDIT_PLACE);
                }
            });
            remove.setPreferredSize(new Dimension(230, 25));
            this.content.add(remove);
        }

        JButton save = new JButton("Save changes");
        save.addActionListener(e -> {
            detail.setPlaceName(placeName.getText());
            detail.setAddress(placeAddress.getText());
            detail.setDescription(placeDescription.getText());
            PlaceEditMap map = (PlaceEditMap) parent.getContentLayout().getMap();
            detail.setCoordinates(map.getPoints());
            if (parent.getController().addOrUpdatePlace(detail)) {
                JOptionPane.showMessageDialog(parent, "Location saved successfully.", "Success", JOptionPane.INFORMATION_MESSAGE);
                parent.getController().resetPlace();
                parent.updateFrame(Page.EDIT_PLACE);
            } else {
                JOptionPane.showMessageDialog(parent, "Unable to save location.", "Failure", JOptionPane.ERROR_MESSAGE);
            }
        });
        save.setPreferredSize(new Dimension(230, 35));
        this.content.add(save);
    }

    private void generatePlaceDescription() {
        JLabel desc = new SideMenuLabel("Location description");
        this.content.add(desc);

        placeDescription = new JTextArea();
        placeDescription.setLineWrap(true);
        placeDescription.setRows(20);
        placeDescription.setColumns(20);
        placeDescription.setText(parent.getController().getLoadedPlace().getDescription());

        this.content.add(new JScrollPane(placeDescription));
    }

    private void generateCoordinates() {
        JLabel placeNameLabel = new SideMenuLabel("Address");
        this.content.add(placeNameLabel);

        placeAddress = new JTextField(20);
        placeAddress.setText(parent.getController().getLoadedPlace().getAddress());
        placeAddress.setMargin(new Insets(2, 5, 2, 5));

        this.content.add(placeAddress);
    }

    public void setAddress(String address) {
        this.placeAddress.setText(address);
    }
}
