package vutbr.fit.gui.view.menu;

import vutbr.fit.gui.model.PlaceListModel;
import vutbr.fit.gui.view.MapApplication;

import java.awt.*;

/**
 * UserPlacesMenu class represents the side menu containing the list of places a user has access to.
 */
public class UserPlacesMenu extends SideMenu {
    public UserPlacesMenu(MapApplication parent) {
        super(parent);
        SideMenuLabel header = new SideMenuLabel("My places");
        this.content.add(header);
        generatePlaces();
    }

    private void generatePlaces() {
        int listHeight = 0;
        for (PlaceListModel model: parent.getController().getUserPlaces())
            listHeight += generatePlaceListItem(model);
        this.content.setPreferredSize(new Dimension(250, listHeight + 150));
    }
}
