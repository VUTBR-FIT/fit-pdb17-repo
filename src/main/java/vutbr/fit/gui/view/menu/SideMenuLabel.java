package vutbr.fit.gui.view.menu;

import javax.swing.*;
import java.awt.*;


public class SideMenuLabel extends JLabel {
    public SideMenuLabel(String text) {
        super(text);
        this.setFont(new Font("Tahoma", Font.BOLD, 13));
        this.setAlignmentX(CENTER_ALIGNMENT);
        this.setPreferredSize(new Dimension(225, 30));
    }
}

class SearchResultLabel extends JLabel {
    SearchResultLabel(String text) {
        super(text);
        this.setFont(new Font("Tahoma", Font.BOLD, 14));
        this.setAlignmentX(LEFT_ALIGNMENT);
        this.setPreferredSize(new Dimension(220, 30));
    }
}

class AddressLabel extends JLabel {
    AddressLabel(String text) {
        super(text);
        this.setAlignmentX(LEFT_ALIGNMENT);
        this.setFont(new Font("Tahoma", Font.PLAIN, 12));
        this.setPreferredSize(new Dimension(220, 20));
    }
}

class LongLabel extends AddressLabel {

    LongLabel(String text) {
        super("<html><p>" + text + "</p></html>");
        this.setPreferredSize(new Dimension(220, 150));
        this.setVerticalAlignment(JLabel.TOP);
        this.setVerticalTextPosition(JLabel.TOP);
        this.setAlignmentY(TOP_ALIGNMENT);
    }
}

class SideMenuAttribute extends JLabel {
    SideMenuAttribute(String text) {
        super(text);
        this.setPreferredSize(new Dimension(235, 20));

    }
}