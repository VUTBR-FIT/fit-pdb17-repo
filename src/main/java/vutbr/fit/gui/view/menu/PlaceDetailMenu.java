package vutbr.fit.gui.view.menu;

import vutbr.fit.gui.model.CommentModel;
import vutbr.fit.gui.model.EventListModel;
import vutbr.fit.gui.model.PlaceDetailModel;
import vutbr.fit.gui.view.MapApplication;
import vutbr.fit.gui.view.Page;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;

import static vutbr.fit.gui.view.menu.EventDetailMenu.getCommentPanel;

/**
 * PlaceDetailMenu represents the side menu containing detailed information about a place, such as comments.
 */
public class PlaceDetailMenu extends SideMenu {
    private PlaceDetailModel detail;
    private JPanel commentPanel;
    private JPanel eventPanel;

    public PlaceDetailMenu(MapApplication parent) {
        super(parent);
    }

    public PlaceDetailMenu(final MapApplication parent, PlaceDetailModel detail) {
        super(parent);
        this.content.setLayout(new BorderLayout());
        this.detail = detail;

        generateComments();
        generateEvents();

        parent.getController().idPlace = parent.getController().getLoadedPlace().getId();

        final String placeAbout = "About";
        final String placeEvents = "Events";
        final String placeComments = "Comments";

        JScrollPane eventScroll = new JScrollPane(this.eventPanel);
        eventScroll.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
        JScrollPane commentScroll = new JScrollPane(this.commentPanel);
        commentScroll.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);

        JTabbedPane pane = new JTabbedPane();
        pane.add(generateAbout(), placeAbout);
        pane.add(eventScroll, placeEvents);
        pane.add(commentScroll, placeComments);
        this.content.add(pane, BorderLayout.CENTER);
    }

    private void generateEvents() {
        this.eventPanel = new JPanel(new GridBagLayout());
        GridBagConstraints c = new GridBagConstraints();
        c.gridwidth = GridBagConstraints.REMAINDER;
        for (EventListModel model: parent.getController().getEventList()) {
            JPanel temp = generateEvent(model);
            this.eventPanel.add(temp, c);
        }
    }

    private JPanel generateEvent(final EventListModel eventListModel) {
        JPanel panel = new JPanel(new GridBagLayout());

        GridBagConstraints gbc = new GridBagConstraints();
        gbc.gridx = 0;
        gbc.gridy = 0;

        JLabel title = new SearchResultLabel(eventListModel.getTitle());
        panel.add(title, gbc);

        JLabel text = new LongLabel(eventListModel.getText());
        gbc.gridy = 1;
        panel.add(text, gbc);

        JPanel duration = new JPanel(new FlowLayout());
        duration.setAlignmentX(LEFT_ALIGNMENT);
        JLabel label = new JLabel("Duration: ");
        duration.add(label);
        label = new JLabel(eventListModel.getDuration());
        label.setFont(new Font(label.getFont().getFontName(), Font.ITALIC, label.getFont().getSize()));
        duration.add(label);
        gbc.gridy = 2;
        panel.add(duration, gbc);

        JButton details = new JButton("See details »");
        details.addActionListener(e -> {
            parent.getController().loadEvent(eventListModel);
            parent.updateFrame(Page.DETAIL_EVENT);
        });
        gbc.gridy = 3;
        panel.add(details, gbc);

        panel.setBorder(new EmptyBorder(5, 10, 5, 5));

        return panel;
    }

    private JScrollPane generateAbout() {
        JPanel aboutPanel = new JPanel(new BorderLayout());
        JLabel header = new SideMenuLabel(this.detail.getPlaceName());
        aboutPanel.add(header, BorderLayout.NORTH);
        JScrollPane aboutScroll = new JScrollPane(aboutPanel);
        JLabel aboutLabel = new LongLabel(this.detail.getDescription());
        aboutLabel.setPreferredSize(new Dimension(225, this.parent.getContentPane().getHeight() - 150));
        aboutPanel.setBorder(new EmptyBorder(5, 10, 5, 5));
        aboutPanel.add(aboutLabel, BorderLayout.CENTER);
        return aboutScroll;
    }

    private void generateComments() {
        this.commentPanel = new JPanel(new GridBagLayout());
        GridBagConstraints gbc = new GridBagConstraints();
        gbc.gridwidth = GridBagConstraints.REMAINDER;

        for (CommentModel comment: parent.getController().getPlaceComments()) {
            JPanel temp = generateComment(comment);
            commentPanel.add(temp, gbc);
        }
    }

    private JPanel generateComment(CommentModel comment) {
        return getCommentPanel(comment);
    }
}
