package vutbr.fit.gui.view.menu;

import vutbr.fit.gui.model.PlaceListModel;
import vutbr.fit.gui.view.MapApplication;

import java.awt.*;

/**
 * UserFavouriteMenu represents the side menu containing the list of a user's favourited places.
 */
public class UserFavouriteMenu extends SideMenu {
    public UserFavouriteMenu(MapApplication parent) {
        super(parent);
        SideMenuLabel header = new SideMenuLabel("Favourite places");
        this.content.add(header);
        generateFavourites();
    }

    private void generateFavourites() {
        int listHeight = 0;
        for (PlaceListModel model: parent.getController().getUserFavourites())
            listHeight += generatePlaceListItem(model);
        this.content.setPreferredSize(new Dimension(250, listHeight + 150));
    }
}
