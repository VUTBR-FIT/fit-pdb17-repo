package vutbr.fit.gui.view.menu;

import vutbr.fit.gui.view.MapApplication;
import vutbr.fit.gui.view.Page;
import vutbr.fit.gui.view.map.SearchMap;

import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.swing.filechooser.FileFilter;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.io.File;

/**
 * SearchMenu class represents the side menu containing search criteria.
 */
public class SearchMenu extends SideMenu {
    private JButton chooseFile;
    private JTextField placeName;
    private File upload;
    private JTextField startingAddress;

    public SearchMenu(final MapApplication parent) {
        super(parent);
        upload = null;
        SideMenuLabel header = new SideMenuLabel("Search");
        this.content.add(header);
        generateSearchParameters(parent);
    }

    private void generateSearchParameters(final MapApplication parent) {
        //button group
        ButtonGroup buttonGroup = new ButtonGroup();
        JRadioButton addressButton = new JRadioButton("By address");
        addressButton.setSelected(true);
        addressButton.addActionListener(e -> setSearchType(true));
        buttonGroup.add(addressButton);
        JRadioButton photoButton = new JRadioButton("By photography");
        photoButton.addActionListener(e -> setSearchType(false));
        buttonGroup.add(photoButton);

        JLabel startingLabel = new JLabel("Starting address (required)", SwingConstants.LEFT);
        startingLabel.setPreferredSize(new Dimension(235, 20));
        this.content.add(startingLabel);
        startingAddress = new JTextField(20);
        startingAddress.setMargin(new Insets(2, 5, 2, 5));
        startingAddress.addFocusListener(new FocusAdapter() {
            @Override
            public void focusLost(FocusEvent e) {
                if ("".equals(startingAddress.getText()))
                    return;
                SearchMap map = (SearchMap) parent.getContentLayout().getMap();
                map.setStartingAddress(startingAddress.getText());
            }
        });
        this.content.add(startingAddress);

        startingLabel = new JLabel("Search radius (km)", SwingConstants.LEFT);
        startingLabel.setPreferredSize(new Dimension(235, 20));
        this.content.add(startingLabel);

        JTextField radiusValue = new JTextField(20);
        radiusValue.setMargin(new Insets(2, 5, 2, 5));
        this.content.add(radiusValue);

        JPanel radioPanel = new JPanel(new GridBagLayout());
        GridBagConstraints c = new GridBagConstraints();
        c.gridx = 0;
        c.gridy = 0;
        c.anchor = GridBagConstraints.LINE_START;

        JPanel searchPanel = generateAddressPanel(addressButton);
        radioPanel.add(searchPanel,c);

        c.gridy = 2;
        searchPanel = generatePhotoPanel(parent, photoButton);
        radioPanel.add(searchPanel, c);

        this.content.add(radioPanel);


        JButton search = new JButton("Search");
        search.setPreferredSize(new Dimension(230, 35));
        search.setAlignmentY(SwingConstants.SOUTH);
        search.addActionListener((ActionEvent e) -> {
            if ("".equals(startingAddress.getText())) {
                JOptionPane.showMessageDialog(null,
                        "Please enter starting address to search.",
                        "No starting address",
                        JOptionPane.ERROR_MESSAGE);
                return;
            }
            try {
                parent.getController().setRadius((int) Float.parseFloat(radiusValue.getText()));
            } catch (NumberFormatException nfe) {
                parent.getController().setRadius(-1);
            }
            SearchMap map = (SearchMap) parent.getContentLayout().getMap();
            parent.getController().setStartingPoint(map.getStartingPoint());
            if (placeName.isEnabled())
                parent.getController().searchByName(placeName.getText());
            else
                parent.getController().searchByImage(startingAddress.getText(), upload);
            parent.updateFrame(Page.SEARCH_RESULTS);
        });
        this.content.add(search);
    }

    private JPanel generateAddressPanel(JRadioButton addressButton) {
        JPanel searchPanel = new JPanel(new GridBagLayout());
        searchPanel.setBorder(new EmptyBorder(7, 10, 7, 5));
        GridBagConstraints gdc = new GridBagConstraints();
        gdc.gridx = 0;
        gdc.gridy = 0;
        gdc.anchor = GridBagConstraints.LINE_START;
        searchPanel.add(addressButton, gdc);

        JLabel eventLabel = new JLabel("Location name");
        eventLabel.setBorder(new EmptyBorder(2, 3, 3, 0));
        gdc.gridy = 1;
        searchPanel.add(eventLabel, gdc);

        placeName = new JTextField(20);
        placeName.setMargin(new Insets(2, 5, 2, 5));
        gdc.gridy = 2;
        searchPanel.add(placeName, gdc);
        return searchPanel;
    }

    private JPanel generatePhotoPanel(final MapApplication parent, JRadioButton photoButton) {
        JPanel searchPanel;
        GridBagConstraints gdc;
        searchPanel = new JPanel(new GridBagLayout());
        searchPanel.setBorder(new EmptyBorder(7, 10, 7, 5));
        gdc = new GridBagConstraints();
        gdc.gridx = 0;
        gdc.gridy = 0;
        searchPanel.add(photoButton, gdc);

        chooseFile = new JButton("Browse");
        chooseFile.setEnabled(false);
        gdc.gridy = 1;
        gdc.gridwidth = 2;
        final JLabel label = new JLabel("Choose file");
        label.setPreferredSize(new Dimension(150, 30));

        JPanel browseMenu = new JPanel(new FlowLayout());
        browseMenu.setAlignmentX(FlowLayout.LEFT);
        browseMenu.add(chooseFile);
        browseMenu.add(label, "sg label");

        searchPanel.add(browseMenu, gdc);
        chooseFile.addActionListener(e -> {
            JFileChooser fileChooser = new JFileChooser();
            FileFilter imageFilter = new FileNameExtensionFilter(
                    "Image files", ImageIO.getReaderFileSuffixes());
            fileChooser.setFileFilter(imageFilter);
            int retval = fileChooser.showOpenDialog(parent);
            if (retval == JFileChooser.APPROVE_OPTION) {
                upload = fileChooser.getSelectedFile();
            }
            label.setText(String.valueOf(fileChooser.getSelectedFile()));
        });
        return searchPanel;
    }

    private void setSearchType(boolean byAddress) {
        if (byAddress) {
            chooseFile.setEnabled(false);
            placeName.setEnabled(true);
        } else {
            chooseFile.setEnabled(true);
            placeName.setEnabled(false);
        }
    }

    public void setStartAddress(String address) {
        this.startingAddress.setText(address);
    }
}
