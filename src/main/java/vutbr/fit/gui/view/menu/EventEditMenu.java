package vutbr.fit.gui.view.menu;

import jdk.nashorn.internal.scripts.JO;
import vutbr.fit.gui.model.EventDetailModel;
import vutbr.fit.gui.model.EventListModel;
import vutbr.fit.gui.model.PlaceListModel;
import vutbr.fit.gui.view.MapApplication;
import vutbr.fit.gui.view.Page;

import javax.swing.*;
import java.awt.*;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * EventEditMenu class represents the side menu containing form used to add or edit events.
 */
public class EventEditMenu extends SideMenu {
    private EventListModel detail;
    private JTextField eventName;
    private JTextArea eventDescription;
    private JTextField start;
    private JTextField end;

    EventEditMenu(MapApplication parent) {
        super(parent);
    }

    public EventEditMenu(final MapApplication parent, final EventDetailModel detail) {
        super(parent);
        this.detail = detail;

        JLabel header = new SideMenuLabel((detail.getId() > 0 ? "Edit" : "Add") + " event");
        this.content.add(header);

        JLabel label = new SideMenuLabel("Event name");
        this.content.add(label);

        eventName = new JTextField(20);
        eventName.setMargin(new Insets(2, 5, 2, 5));
        eventName.setText(detail.getTitle());
        this.content.add(eventName);

        label = new SideMenuLabel("Event duration (dd.mm.YYYY)");
        this.content.add(label);
        generateDateFields();

        label = new SideMenuLabel("Event description");
        this.content.add(label);
        generateEventDescription();

        if (detail.getId() > 0) {
            JButton delete = new JButton("Delete event");
            delete.addActionListener(e -> {
                int response = JOptionPane.showConfirmDialog(parent,
                        "Are you sure you want to delete this event?",
                        "Delete event?",
                        JOptionPane.YES_NO_OPTION);
                if (response == JOptionPane.YES_OPTION) {
                    parent.getController().removeEvent(detail);
                    JOptionPane.showConfirmDialog(parent, "Event successfully deleted.", "Event deleted",
                            JOptionPane.INFORMATION_MESSAGE);
                    parent.getController().resetEvent();
                    parent.getController().getLoadedEvent().setPlaceId(detail.getPlaceId());
                    parent.updateFrame(Page.EDIT_EVENT);
                }
            });
            delete.setPreferredSize(new Dimension(230, 25));
            this.content.add(delete);
        }

        JButton save = new JButton("Save changes");
        save.addActionListener(e -> {
            detail.setTitle(eventName.getText());
            detail.setText(eventDescription.getText());
            detail.setStart(start.getText());
            detail.setEnd(start.getText());
            if (detail.getPlaceId() == 0)
                detail.setPlaceId(parent.getController().getLoadedPlace().getId());
            if (parent.getController().addOrUpdateEvent(detail)) {
                JOptionPane.showMessageDialog(parent, "Event saved successfully.", "Success", JOptionPane.INFORMATION_MESSAGE);
                parent.getController().resetEvent();
                parent.getController().getLoadedEvent().setPlaceId(detail.getPlaceId());
                parent.updateFrame(Page.EDIT_EVENT);
            } else {
                JOptionPane.showMessageDialog(parent, "Event could not be saved.", "Failure", JOptionPane.ERROR_MESSAGE);
            }
        });
        save.setPreferredSize(new Dimension(230, 35));
        this.content.add(save);
    }

    private void generateEventDescription() {
        eventDescription = new JTextArea();
        eventDescription.setLineWrap(true);
        eventDescription.setBorder(new JTextField().getBorder());
        eventDescription.setColumns(20);
        eventDescription.setRows(10);
        eventDescription.setText(parent.getController().getLoadedEvent().getText());
        this.content.add(eventDescription);
    }

    private void generateDateFields() {
        JLabel label;
        JPanel date = new JPanel(new GridBagLayout());
        GridBagConstraints c = new GridBagConstraints();
        c.fill = GridBagConstraints.HORIZONTAL;
        c.gridx = 0;
        c.gridy = 0;
        c.gridwidth = 1;
        label = new JLabel("Start date:   ");
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");
        Date dateObj = new Date();
        date.add(label, c);

        c.gridx = 1;
        c.gridwidth = 2;
        start = new JTextField(13);
        start.setMargin(new Insets(2, 5, 2, 5));
        String value = parent.getController().getLoadedEvent().getStart();
        if (parent.getController().getLoadedEvent().getId() ==0 )
            value = dateFormat.format(dateObj);
        start.setText(value);
        date.add(start, c);

        c.gridy = 1;
        c.gridx = 0;
        c.gridwidth = 1;
        label  = new JLabel("End date:");
        date.add(label, c);

        c.gridx = 1;
        c.gridwidth = 2;
        end = new JTextField(13);
        end.setMargin(new Insets(2, 5, 2, 5));
        value = parent.getController().getLoadedEvent().getEnd();
        if (parent.getController().getLoadedEvent().getId() ==0 )
            value = dateFormat.format(dateObj);
        end.setText(value);
        date.add(end, c);
        this.content.add(date);
    }
}
