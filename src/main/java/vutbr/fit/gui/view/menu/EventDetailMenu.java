package vutbr.fit.gui.view.menu;

import vutbr.fit.gui.model.CommentModel;
import vutbr.fit.gui.model.EventDetailModel;
import vutbr.fit.gui.model.EventListModel;
import vutbr.fit.gui.view.MapApplication;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;

/**
 * EventDetailMenu class represents the side menu containing detail information about an event, such as comments and
 * description.
 */
public class EventDetailMenu extends SideMenu {
    private JPanel aboutPanel;
    private JPanel commentPanel;
    private EventListModel detail;

    public EventDetailMenu(MapApplication parent, EventDetailModel loadedEvent) {
        super(parent);
    }

    public EventDetailMenu(final MapApplication parent, EventListModel detail) {
        super(parent);
        this.detail = detail;
        this.content.setLayout(new BorderLayout());

        generateAbout();
        generateComments();

        final String eventAbout = "About";
        final String eventComments = "Comments";

        JScrollPane aboutScroll = new JScrollPane(this.aboutPanel);
        aboutScroll.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
        JScrollPane commentScroll = new JScrollPane(this.commentPanel);
        commentScroll.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);

        JTabbedPane pane = new JTabbedPane();
        pane.add(aboutScroll, eventAbout);
        pane.add(commentScroll, eventComments);

        this.content.add(pane, BorderLayout.CENTER);
    }

    private void generateAbout() {
        this.aboutPanel = new JPanel(new BorderLayout());
        this.aboutPanel.setBorder(new EmptyBorder(5, 10, 5, 5));
        JPanel top = new JPanel(new GridLayout(0, 1));

        JLabel header = new SideMenuLabel(this.detail.getTitle());
        top.add(header);

        JPanel duration = new JPanel(new FlowLayout());
        duration.setAlignmentX(LEFT_ALIGNMENT);
        JLabel label = new JLabel("Duration: ");
        duration.add(label);
        label = new JLabel(detail.getDuration());
        label.setFont(new Font(label.getFont().getFontName(), Font.ITALIC, label.getFont().getSize()));
        duration.add(label);
        top.add(duration);

        this.aboutPanel.add(top, BorderLayout.NORTH);

        JLabel aboutLabel = new LongLabel(detail.getText());
        this.aboutPanel.add(aboutLabel, BorderLayout.CENTER);
    }

    private void generateComments() {
        this.commentPanel = new JPanel(new GridBagLayout());
        GridBagConstraints gbc = new GridBagConstraints();
        gbc.gridwidth = GridBagConstraints.REMAINDER;

        for (CommentModel comment: parent.getController().getEventComments()) {
            JPanel temp = generateComment(comment);
            commentPanel.add(temp, gbc);
        }
    }

    private JPanel generateComment(CommentModel comment) {
        return getCommentPanel(comment);
    }

    static JPanel getCommentPanel(CommentModel comment) {
        JPanel panel = new JPanel(new GridBagLayout());

        GridBagConstraints gbc = new GridBagConstraints();
        gbc.gridx = 0;
        gbc.gridy = 0;

        JLabel header = new SearchResultLabel(comment.getTitle());
        panel.add(header, gbc);

        gbc.gridy = 1;
        JLabel text = new LongLabel(comment.getMessageText());
        panel.add(text, gbc);

        gbc.gridy = 2;
        JPanel username = new JPanel(new FlowLayout());
        username.setAlignmentX(FlowLayout.LEFT);
        JLabel label = new JLabel("By ");
        username.add(label);
        label = new JLabel(comment.getUsername());
        label.setFont(new Font(label.getFont().getFontName(), Font.ITALIC, label.getFont().getSize()));
        username.add(label);
        panel.add(username, gbc);

        panel.setBorder(new EmptyBorder(5, 10, 5, 5));
        return panel;
    }
}
