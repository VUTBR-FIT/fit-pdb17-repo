package vutbr.fit.gui.view.menu;

import vutbr.fit.gui.model.PlaceListModel;
import vutbr.fit.gui.view.MapApplication;

import java.awt.*;

/**
 * SearchResultMenu class represents the side menu containing all place search results.
 */
public class SearchResultMenu extends SideMenu {

    public SearchResultMenu(MapApplication parent) {
        super(parent);
        SideMenuLabel header = new SideMenuLabel("Search results");
        this.content.add(header);
        generateResults();
    }

    private void generateResults() {
        int listHeight = 0;
        for (PlaceListModel model: parent.getController().getSearchResults())
            listHeight += generatePlaceListItem(model);
        this.content.setPreferredSize(new Dimension(250, listHeight + 150));
    }
}
