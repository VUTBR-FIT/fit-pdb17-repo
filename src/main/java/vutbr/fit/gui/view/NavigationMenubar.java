package vutbr.fit.gui.view;

import vutbr.fit.gui.model.ImageModel;
import vutbr.fit.gui.view.misc.ImageCropPanel;
import vutbr.fit.gui.view.misc.TransparentButton;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * NavigationMenubar represents the menu bar present in the content area used to partially navigate thorough the application.
 */
class NavigationMenubar extends JMenuBar {
    private MapApplication parent;
    private Page pageType;
    NavigationMenubar(MapApplication parent, Page type) {
        super();
        this.parent = parent;
        this.pageType = type;
        generateButtons();
        this.setPreferredSize(new Dimension(parent.getContentPane().getWidth() - 252, this.getPreferredSize().height));
    }

    private void generateButtons() {
        JButton button;
        switch (pageType) {
            case SEARCH_RESULTS:
                button = new TransparentButton("« Back to search");
                button.addActionListener(e -> {
                    parent.getController().resetSession();
                    parent.updateFrame(Page.SEARCH_PAGE);
                });
                this.add(button);
                this.add(Box.createHorizontalGlue());
                break;
            case DETAIL_PLACE:
                button = new TransparentButton("« Back to search results");
                button.addActionListener(e -> parent.updateFrame(Page.SEARCH_RESULTS));
                this.add(button);
                generateFavouriteButton();
                this.add(Box.createHorizontalGlue());

                generatePhotoNavigation();
                break;
            case DETAIL_EVENT:
                button = new TransparentButton("« Back to location detail");
                button.addActionListener(e -> parent.updateFrame(Page.SEARCH_RESULTS));
                this.add(button);
                generateFavouriteButton();
                this.add(Box.createHorizontalGlue());

                generatePhotoNavigation();
                break;
            default:
                break;
        }
    }

    private void generateFavouriteButton() {
        if ("".equals(parent.getUsername()))
            return;
        JButton button;
        if (parent.getController().isPlaceFavourite())
            button = new TransparentButton("Remove favourite");
        else
            button = new TransparentButton("Add favourite");
        button.addActionListener(e -> {
            parent.getController().toggleFavourite();
            if (parent.getController().isPlaceFavourite())
                button.setText("Remove favourite");
            else
                button.setText("Add favourite");
        });
        this.add(button);
    }

    private void generatePhotoNavigation() {
        if (pageType == Page.DETAIL_EVENT && parent.getController().getLoadedEvent().getImages().size() == 0 ||
                pageType == Page.DETAIL_PLACE && parent.getController().getLoadedPlace().getImages().size() == 0)
            return;

        JButton button;

        button = new TransparentButton("‹ Previous photo");
        button.addActionListener(e -> parent.getContentLayout().previousPicture());
        this.add(button);

        if (parent.getController().testPhotoOwnership(parent.getContentLayout().getCurrentPhoto())) {
            button = new TransparentButton("Edit photo");
            button.addActionListener(new ActionListener() {
                ImageModel image = parent.getContentLayout().getCurrentPhoto();
                ImageCropPanel panel = new ImageCropPanel(image.image);
                @Override
                public void actionPerformed(ActionEvent e) {
                    String options[] = {"Save", "Remove", "Cancel"};
                    int result = JOptionPane.showOptionDialog(null, new JComponent[]{ panel },
                            "Edit photo", JOptionPane.DEFAULT_OPTION, JOptionPane.PLAIN_MESSAGE, null,
                            options, options[0]);
                    if (result == 0) {
                        image.setImage(panel.getCroppedImage());
                        parent.getController().addOrUpdatePhoto(image);
                    }
                    else if (result == 1) {
                        int deleteResult = JOptionPane.showConfirmDialog(null, "Are you sure you want to delete this photo?");
                        if (deleteResult == JOptionPane.OK_OPTION) {
                            parent.getController().removeImage(image);
                            parent.getContentLayout().initSlideshow(); // reload after delete
                        }
                    }
                }
            });
            this.add(button);
        }

        button = new TransparentButton("Next photo ›");
        button.addActionListener(e -> parent.getContentLayout().nextPicture());
        this.add(button);
    }
}
