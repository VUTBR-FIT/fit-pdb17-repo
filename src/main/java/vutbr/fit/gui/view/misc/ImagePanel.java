package vutbr.fit.gui.view.misc;

import vutbr.fit.gui.model.ImageModel;
import vutbr.fit.gui.view.MapApplication;

import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
/**
 * ImagePanel class represents the panel containing upload button and crop panel used to upload user photos.
 */
public class ImagePanel extends JPanel {
    private final ImageModel imageModel;
    private BufferedImage image;
    private double ratio;

    public ImagePanel(MapApplication parent, ImageModel model) {
        this.setPreferredSize(new Dimension(parent.getContentPane().getWidth() - 250, parent.getContentPane().getHeight()));
        this.imageModel = model;
        this.image = model.getImage();
        this.setLayout(new BorderLayout());
        double width_ratio = (parent.getWidth() - 250) / image.getWidth();
        double height_ratio = parent.getHeight() / image.getHeight();
        ratio = Math.min(width_ratio, height_ratio);
    }

    @Override
    protected void paintComponent (Graphics g) {
        super.paintComponent(g);
        int width = (int)(this.image.getWidth() * ratio);
        int height = (int)(this.image.getHeight() * ratio);
        g.drawImage(this.image, (this.getPreferredSize().width - width) / 2, (this.getPreferredSize().height - height) / 2,
                width, height, null);
    }


}
