package vutbr.fit.gui.view.misc;

import vutbr.fit.gui.view.MapApplication;

import javax.swing.*;
import java.awt.*;

/**
 * RegistrerDialog class represents the registration pop-up.
 */
public class RegisterDialog extends JDialog {

    public RegisterDialog(final MapApplication parent) {
        super(parent, "Register", true);
        setSize(400, 150);

        JPanel panel = new JPanel(new GridBagLayout());
        GridBagConstraints c = new GridBagConstraints();
        c.fill = GridBagConstraints.HORIZONTAL;

        JLabel label = new JLabel("Name:       ");
        c.gridx = 0;
        panel.add(label, c);

        final JTextField firstName = new JTextField(20);
        c.gridx = 1;
        panel.add(firstName, c);

        label = new JLabel("Surname: ");
        c.gridx = 0;
        c.gridy = 1;
        panel.add(label, c);

        final JTextField lastName = new JTextField(20);
        c.gridx = 1;
        panel.add(lastName, c);

        label = new JLabel("E-mail: ");
        c.gridx = 0;
        c.gridy = 2;
        panel.add(label, c);

        final JTextField emailAddress = new JTextField(20);
        c.gridx = 1;
        c.gridwidth = 2;
        panel.add(emailAddress, c);

        label = new JLabel("Password: ");
        c.gridx = 0;
        c.gridy = 3;
        panel.add(label, c);

        final JPasswordField passwd = new JPasswordField(20);
        passwd.setText("");
        c.gridx = 1;
        c.gridwidth = 2;
        panel.add(passwd, c);

        JButton login = new JButton("Confirm");
        login.addActionListener(e -> {
            if (parent.getController().registerUser(firstName.getText(), lastName.getText(), emailAddress.getText(),
                    new String(passwd.getPassword()))) {
                JOptionPane.showMessageDialog(parent, "Registration successful.");
                setVisible(false);
                dispose();
            } else {
                JOptionPane.showMessageDialog(RegisterDialog.this,
                        "Username already taken",
                        "Unable to create account",
                        JOptionPane.ERROR_MESSAGE);
                // reset emailAddress
                emailAddress.setText("");
            }
        });
        c.gridwidth = 3;
        c.gridx = 0;
        c.gridy = 4;
        panel.add(login, c);

        getContentPane().add(panel, BorderLayout.CENTER);
        pack();
        setResizable(false);
        setLocationRelativeTo(parent);
    }
}
