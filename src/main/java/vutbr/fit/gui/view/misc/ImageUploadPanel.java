package vutbr.fit.gui.view.misc;

import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.filechooser.FileFilter;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;

/**
 * ImageUploadPanel represents the panel used to upload photos.
 */
public class ImageUploadPanel extends JPanel{
    private File uploadedFile;
    private ImageCropPanel cropPanel;
    public ImageUploadPanel() {
        this.setLayout(new BorderLayout());
        generateBrowsePanel();
        this.cropPanel = new ImageCropPanel();
        this.add(this.cropPanel, BorderLayout.CENTER);
    }

    private void generateBrowsePanel() {
        JPanel browsePanel = new JPanel(new FlowLayout());

        JButton chooseFile = new JButton("Browse");
        final JLabel label = new JLabel("Choose file");

        browsePanel.setAlignmentX(FlowLayout.LEFT);
        browsePanel.add(chooseFile);
        browsePanel.add(label, "sg label");

        chooseFile.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JFileChooser fileChooser = new JFileChooser();
                FileFilter imageFilter = new FileNameExtensionFilter(
                        "Image files", ImageIO.getReaderFileSuffixes());
                fileChooser.setFileFilter(imageFilter);
                int retval = fileChooser.showOpenDialog(null);
                if (retval == JFileChooser.APPROVE_OPTION) {
                    uploadedFile = fileChooser.getSelectedFile();
                    try {
                        cropPanel = new ImageCropPanel(uploadedFile);
                        add(cropPanel, BorderLayout.CENTER); // mozno bug?
                    } catch (IOException e1) {
                        e1.printStackTrace();
                    }
                }
                label.setText(String.valueOf(uploadedFile));
            }
        });
        this.add(browsePanel, BorderLayout.NORTH);
    }

    public ImageCropPanel getCropPanel() {
        return cropPanel;
    }
}
