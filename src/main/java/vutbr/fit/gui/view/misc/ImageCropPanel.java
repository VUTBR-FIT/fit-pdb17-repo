package vutbr.fit.gui.view.misc;

import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

/**
 * ImageCropPanel class represents the panel used to crop uploaded images.
 */
public class ImageCropPanel extends JPanel {
    private Point startPoint;
    private Point endPoint;
    private BufferedImage loadedImage;
    private BufferedImage croppedImage;
    private double ratio;

    private int start_x, start_y, width, height;

    ImageCropPanel() {
        super();
        this.setPreferredSize(new Dimension(800, 600));
        this.setBorder(new EmptyBorder(0, 0, 0, 50));
        this.startPoint = new Point(0, 0);
        this.endPoint = new Point(0, 0);
    }

    public ImageCropPanel(final BufferedImage image) {
        this();
        this.loadedImage = image;
        this.croppedImage = this.loadedImage;

        initialize();
    }

    ImageCropPanel(final File imageFile) throws IOException {
        this();
        this.loadedImage = ImageIO.read(imageFile);
        this.croppedImage = this.loadedImage;

        initialize();
    }

    private void initialize() {
        repaint();

        double widthRatio = 800. / loadedImage.getWidth();
        double heightRatio = 600. / loadedImage.getHeight();
        this.ratio = Math.min(widthRatio, heightRatio);

        this.endPoint = new Point((int)(this.loadedImage.getWidth() * ratio), (int)(this.loadedImage.getHeight() * ratio));

        this.addMouseListener(new MouseAdapter() {
            @Override
            public void mousePressed(MouseEvent e) {
                startPoint = new Point(e.getX(), e.getY());
                System.out.println(startPoint.x + " " + startPoint.y);
                endPoint = startPoint;
                repaint();
            }

            public void mouseReleased(MouseEvent e) {
                try {
                    croppedImage = loadedImage.getSubimage(start_x, start_y, width, height);
                } catch (java.awt.image.RasterFormatException ignored) {
                    ignored.printStackTrace();
                }
            }
        });

        this.addMouseMotionListener(new MouseMotionAdapter() {
            @Override
            public void mouseDragged(MouseEvent e) {
                endPoint = new Point(e.getX(), e.getY());
                repaint();
            }
        });

        this.setPreferredSize(new Dimension((int)(this.loadedImage.getWidth() * ratio),
                (int) (this.loadedImage.getHeight() * ratio)));
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        if (this.loadedImage == null)
            return;
        start_x = (int) (Math.min(startPoint.x, endPoint.x) / ratio);
        start_y = (int) (Math.min(startPoint.y, endPoint.y) / ratio);
        width = (int) (Math.abs(startPoint.x - endPoint.x) / ratio) + start_x >= this.loadedImage.getWidth() ?
                this.loadedImage.getWidth() - start_x : (int) (Math.abs(startPoint.x - endPoint.x) / ratio);
        height = (int) (Math.abs(startPoint.y - endPoint.y) / ratio) + start_y >= this.loadedImage.getHeight() ?
                this.loadedImage.getHeight() - start_y : (int) (Math.abs(startPoint.y - endPoint.y) / ratio);
        g.drawImage(this.loadedImage, 0, 0, (int)(this.loadedImage.getWidth() * ratio),
                (int)(this.loadedImage.getHeight() * ratio), null);
        if (width <= 0 || height <= 0)
            return;
        g.setColor(Color.RED);
        g.drawRect(Math.min(startPoint.x, endPoint.x),
                Math.min(startPoint.y, endPoint.y),
                Math.abs(startPoint.x - endPoint.x) + 1,
                Math.abs(startPoint.y - endPoint.y) + 1);
    }

    public BufferedImage getCroppedImage() {
        return croppedImage;
    }
}
