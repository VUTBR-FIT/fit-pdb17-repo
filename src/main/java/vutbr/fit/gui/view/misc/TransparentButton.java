package vutbr.fit.gui.view.misc;

import javax.swing.*;
import java.awt.*;

public class TransparentButton extends JButton {
    public TransparentButton(String text) {
        super(text);
        this.setFocusPainted(false);
        this.setContentAreaFilled(false);
        this.setBackground(null);
        this.setBorderPainted(false);
        setMargin(new Insets(0, 5, 0, 5));
        this.setMinimumSize(new JMenuItem(text).getPreferredSize());
    }
}
