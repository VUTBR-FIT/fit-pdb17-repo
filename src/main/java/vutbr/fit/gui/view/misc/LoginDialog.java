package vutbr.fit.gui.view.misc;

import vutbr.fit.gui.view.MapApplication;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * LoginDialog class represents the login pop-up.
 */
public class LoginDialog extends JDialog {

    public LoginDialog(final MapApplication parent) {
        super(parent, "Login", true);
        setSize(400, 150);

        JPanel panel = new JPanel(new GridBagLayout());
        GridBagConstraints c = new GridBagConstraints();
        c.fill = GridBagConstraints.HORIZONTAL;

        JLabel label = new JLabel("E-mail: ");
        c.gridx = 0;
        c.gridy = 0;
        c.gridwidth = 1;
        panel.add(label, c);

        final JTextField username = new JTextField(20);
        username.setText("");
        c.gridx = 1;
        c.gridy = 0;
        c.gridwidth = 2;
        panel.add(username, c);

        label = new JLabel("Password: ");
        c.gridx = 0;
        c.gridy = 1;
        c.gridwidth = 1;
        panel.add(label, c);

        final JPasswordField passwd = new JPasswordField(20);
        passwd.setText("");
        c.gridx = 1;
        c.gridwidth = 2;
        panel.add(passwd, c);

        JButton login = new JButton("Login");
        login.addActionListener(e -> {
            if (parent.authenticateUser(username.getText(), passwd.getPassword())) {
                setVisible(false);
                dispose();
            } else {
                JOptionPane.showMessageDialog(LoginDialog.this,
                        "Invalid username or password",
                        "Login",
                        JOptionPane.ERROR_MESSAGE);
                // reset username and password
                username.setText("");
                passwd.setText("");
            }
        });
        c.gridwidth = 3;
        c.gridx = 0;
        c.gridy = 2;
        panel.add(login, c);

        getContentPane().add(panel, BorderLayout.CENTER);
        pack();
        setResizable(false);
        setLocationRelativeTo(parent);
    }
}
