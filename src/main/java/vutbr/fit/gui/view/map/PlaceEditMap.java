package vutbr.fit.gui.view.map;

import com.teamdev.jxmaps.*;
import vutbr.fit.gui.model.CoordinatesModel;
import vutbr.fit.gui.view.MapApplication;
import vutbr.fit.gui.view.Page;
import vutbr.fit.gui.view.menu.PlaceEditMenu;

import java.util.ArrayList;

import static com.teamdev.jxmaps.GeocoderStatus.OK;

/**
 * PlaceEditMap is responsible for generating map containing markers that represent individual place vertices and their
 * manipulation.
 */
public class PlaceEditMap extends InteractiveMap {
    private ArrayList<LatLng> placeCoords;
    private ArrayList<Marker> markers;
    private Polygon polygon;
    private Marker centerMarker;
    private LatLng center;

    public PlaceEditMap(MapViewOptions options, MapApplication parent, Page pageType) {
        super(options, parent, pageType);
        polygon = null;
        centerMarker = null;
        center = null;

        setOnMapReadyHandler(status -> {
            if (status != MapStatus.MAP_STATUS_OK)
                return;

            this.loaded = true;
            map = getMap();
            map.setZoom(12.0);
            GeocoderRequest request = new GeocoderRequest();
            request.setAddress("Brno");

            // set map center to Brno
            getServices().getGeocoder().geocode(request, new GeocoderCallback(map) {
                @Override
                public void onComplete(GeocoderResult[] result, GeocoderStatus status) {
                    if (status != OK)
                        return;
                    map.setCenter(result[0].getGeometry().getLocation());
                }
            });

            generateAddressEditor();
        });
    }

    private void generateAddressEditor() {
        placeCoords = new ArrayList<>();
        markers = new ArrayList<>();
        loadExistingPoints();
        if (placeCoords.size() > 0)
            drawPlacePolygon();
        if (center != null)
            map.setCenter(center);

        this.map.addEventListener("click", new MapMouseEvent() {
            @Override
            public void onEvent(MouseEvent mouseEvent) {
                handleMarkerInteraction(mouseEvent);
            }
        });
    }

    private void drawPlacePolygon() {
        if (polygon != null)
            polygon.remove();
        LatLng[] placeCoordsArray = new LatLng[placeCoords.size() + 1];
        for (int i = 0; i < placeCoords.size(); i++) {
            placeCoordsArray[i] = new LatLng(placeCoords.get(i).getLat(), placeCoords.get(i).getLng());
        }
        placeCoordsArray[placeCoords.size()] = placeCoords.get(0);
        polygon = new Polygon(this.map);
        polygon.setPath(placeCoordsArray);
        PolygonOptions options = new PolygonOptions();
        options.setFillColor("#FF0000");
        options.setFillOpacity(0.3);
        options.setStrokeColor("#FF0000");
        options.setStrokeOpacity(0.75);
        options.setStrokeWeight(2.0);
        polygon.setOptions(options);

        polygon.addEventListener("click", new MapMouseEvent() {
            @Override
            public void onEvent(MouseEvent mouseEvent) {
                handleMarkerInteraction(mouseEvent);
            }
        });

        setCenterAddress();
    }

    private void handleMarkerInteraction(MouseEvent mouseEvent) {
        final Marker marker = new Marker(map);
        marker.setPosition(mouseEvent.latLng());
        marker.setOpacity(0.7);
        placeCoords.add(new LatLng(mouseEvent.latLng().getLat(), mouseEvent.latLng().getLng()));
        drawPlacePolygon();
        marker.addEventListener("click", new MapMouseEvent() {
            @Override
            public void onEvent(MouseEvent mouseEvent) {
                marker.remove();
                removePositionMarker(mouseEvent.latLng());
                drawPlacePolygon();
            }
        });
    }

    private void setCenterAddress() {
        center = getCenter(placeCoords);
        GeocoderRequest request = new GeocoderRequest();
        request.setLocation(center);
        getServices().getGeocoder().geocode(request, new GeocoderCallback(map) {
            @Override
            public void onComplete(GeocoderResult[] geocoderResults, GeocoderStatus geocoderStatus) {
                if (geocoderStatus != GeocoderStatus.OK || geocoderResults.length == 0)
                    return;
                GeocoderResult result = geocoderResults[0];
                String currentAddress = result.getFormattedAddress().length() > 0 ? result.getFormattedAddress()
                        : result.getGeometry().getLocation().toString();
                PlaceEditMenu menu = (PlaceEditMenu) parent.getContentLayout().getMenu();
                menu.setAddress(currentAddress);

                if (centerMarker != null)
                    centerMarker.remove();
                centerMarker = new Marker(map);
                centerMarker.setPosition(center);
                InfoWindow window = new InfoWindow(map);
                window.setContent(currentAddress);
                window.open(map, centerMarker);
            }
        });
    }

    private void loadExistingPoints() {
        if (parent.getController().getLoadedPlace().getId() == 0)
            return;

        for (CoordinatesModel coords: parent.getController().getLoadedPlace().getCoordinates()) {
            placeCoords.add(new LatLng(coords.x, coords.y));
        }

        for (LatLng point: placeCoords) {
            final Marker marker = new Marker(map);
            marker.setPosition(new LatLng(point.getLat(), point.getLng()));
            marker.setOpacity(0.7);
            markers.add(marker);
            marker.addEventListener("click", new MapMouseEvent() {
                @Override
                public void onEvent(MouseEvent mouseEvent) {
                    marker.remove();
                    removePositionMarker(mouseEvent.latLng());
                }
            });
        }
        drawPlacePolygon();
    }

    private void removePositionMarker(LatLng position) {
        int index = -1;
        for (int i = 0; i < placeCoords.size(); i++) {
            if (placeCoords.get(i).equals(position))
                index = i;
        }
        if (index > -1)
            placeCoords.remove(index);
    }

    public ArrayList<CoordinatesModel> getPoints() {
        ArrayList<CoordinatesModel> coordinates = new ArrayList<>();
        for (LatLng point: placeCoords) {
            coordinates.add(new CoordinatesModel((float)point.getLat(), (float)point.getLng()));
        }
        return coordinates;
    }
}
