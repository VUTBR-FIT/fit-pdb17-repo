package vutbr.fit.gui.view.map;

import com.teamdev.jxmaps.*;
import vutbr.fit.gui.model.CoordinatesModel;
import vutbr.fit.gui.model.PlaceListModel;
import vutbr.fit.gui.view.MapApplication;
import vutbr.fit.gui.view.Page;

import java.util.ArrayList;

import static com.teamdev.jxmaps.GeocoderStatus.OK;

/**
 * InteractiveMarkerMap class is responsible for creating map that contains markers representing individual places and
 * basic information about them. Each place is represented by a polygon and an info window in its center.
 */
public class InteractiveMarkerMap extends InteractiveMap {
    public InteractiveMarkerMap(MapViewOptions options, MapApplication parent, Page pageType) {
        super(options, parent, pageType);
        setOnMapReadyHandler(status -> {
            if (status != MapStatus.MAP_STATUS_OK)
                return;

            this.loaded = true;
            map = getMap();
            map.setZoom(15.0);
            GeocoderRequest request = new GeocoderRequest();
            request.setAddress("Brno");

            // set map center to Brno
            getServices().getGeocoder().geocode(request, new GeocoderCallback(map) {
                @Override
                public void onComplete(GeocoderResult[] result, GeocoderStatus status) {
                    if (status != OK)
                        return;
                    map.setCenter(result[0].getGeometry().getLocation());
                    generateInteractiveMarkers();
                }
            });
        });
    }

    private void generateDraggableStartingPoint() {
        Marker marker = new Marker(map);
        LatLng position = new LatLng(parent.getController().getStartingPoint().x,
                parent.getController().getStartingPoint().y);
        marker.setPosition(position);
        marker.setOpacity(0.7);
        map.setCenter(position);
        marker.setDraggable(true);
        marker.addEventListener("dragend", new MapMouseEvent() {
            @Override
            public void onEvent(MouseEvent mouseEvent) {
                parent.getController().setStartingPoint(new CoordinatesModel((float) mouseEvent.latLng().getLat(),
                        (float) mouseEvent.latLng().getLng()));
                parent.getController().filterByRadius(parent.getController().getRadius());
                parent.getContentLayout().initMap(pageType);
            }
        });
    }

    private void generateInteractiveMarkers() {
        ArrayList<PlaceListModel> results;
        if (this.pageType == Page.SEARCH_RESULTS)
            results = parent.getController().getSearchResults();
        else if (this.pageType == Page.USER_PLACES)
            results = parent.getController().getUserPlaces();
        else //if (this.pageType == Page.USER_FAVOURITES)
            results = parent.getController().getUserFavourites();
        for (PlaceListModel detail: results) {
            Marker marker = new Marker(map);
            marker.setPosition(getPlaceCenter(detail));
            ArrayList<LatLng> coordinates = new ArrayList<>();
            for (CoordinatesModel point: detail.getCoordinates())
                coordinates.add(new LatLng(point.x, point.y));
            coordinates.add(new LatLng(detail.getCoordinates().get(0).x, detail.getCoordinates().get(0).y));
            LatLng coordinatesArray[] = new LatLng[coordinates.size()];
            coordinatesArray = coordinates.toArray(coordinatesArray);
            generatePolygon(coordinatesArray);
            marker.setPosition(getCenter(coordinates));

            InfoWindow window = new InfoWindow(map);
            window.setContent("<b>" + detail.getPlaceName() + "</b><br />" + detail.getAddress());
            window.open(map, marker);

        }
    }
}
