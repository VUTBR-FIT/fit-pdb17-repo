package vutbr.fit.gui.view.map;

import com.teamdev.jxmaps.*;
import vutbr.fit.gui.model.CoordinatesModel;
import vutbr.fit.gui.model.PlaceListModel;
import vutbr.fit.gui.view.MapApplication;
import vutbr.fit.gui.view.Page;

import java.awt.*;
import java.util.ArrayList;


/**
 * PlainMarkerMap class is responsible for creating a map with a single place and its corresponding marker.
 */
public class PlainMarkerMap extends InteractiveMap {

    public PlainMarkerMap(MapViewOptions options, MapApplication parent, Page pageType) {
        super(options, parent, pageType);


        setOnMapReadyHandler(status -> {
            if (status != MapStatus.MAP_STATUS_OK)
                return;

            this.loaded = true;
            this.map = getMap();
            this.map.setZoom(12.0);
            generateMarkers(parent, pageType);
        });
    }

    private void generateMarkers(MapApplication parent, Page pageType) {
        // EDIT_EVENT can be accessed from the user event list, therefore the correct place needs to be loaded first
        if (pageType == Page.EDIT_EVENT) {
            PlaceListModel model = new PlaceListModel();
            model.setId(parent.getController().getLoadedEvent().getPlaceId());
            parent.getController().loadPlace(model);
        }

        ArrayList<LatLng> placeCoordinates = new ArrayList<>();
        LatLng coordinatesArray[];

        coordinatesArray = loadPlaceCoordinates(parent, placeCoordinates);
        generatePolygon(coordinatesArray);

        LatLng center = getCenter(placeCoordinates);
        Marker centerMarker = new Marker(map);
        centerMarker.setPosition(center);
        InfoWindow window = new InfoWindow(map);
        // DETAIL_PLACE - place itself, DETAIL_EVENT/EDIT_EVENT - place of event
        if (pageType == Page.DETAIL_EVENT)
            window.setContent("<b>" + parent.getController().getLoadedEvent().getTitle() + "</b><br />"
                    + parent.getController().getLoadedPlace().getPlaceName());
        else
            window.setContent("<b>" + parent.getController().getLoadedPlace().getPlaceName() + "</b><br />"
                    + parent.getController().getLoadedPlace().getAddress());
        window.open(map, centerMarker);
        map.setCenter(center);
    }

    private LatLng[] loadPlaceCoordinates(MapApplication parent, ArrayList<LatLng> placeCoordinates) {
        LatLng[] coordinatesArray;
        for (CoordinatesModel coords: parent.getController().getLoadedPlace().getCoordinates())
            placeCoordinates.add(new LatLng(coords.x, coords.y));
        placeCoordinates.add(new LatLng(parent.getController().getLoadedPlace().getCoordinates().get(0).x,
                parent.getController().getLoadedPlace().getCoordinates().get(0).y));
        coordinatesArray = new LatLng[placeCoordinates.size()];
        coordinatesArray = placeCoordinates.toArray(coordinatesArray);
        return coordinatesArray;
    }
}
