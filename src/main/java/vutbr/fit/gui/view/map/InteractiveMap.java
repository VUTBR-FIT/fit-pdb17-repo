package vutbr.fit.gui.view.map;

import com.teamdev.jxmaps.*;
import vutbr.fit.gui.model.CoordinatesModel;
import vutbr.fit.gui.model.PlaceListModel;
import vutbr.fit.gui.view.MapApplication;
import vutbr.fit.gui.view.Page;

import java.util.ArrayList;

/**
 * InteractiveMap is the base class for all maps that are displayed alongside corresponding side menus. It is
 * responsible for initializing a map, and drawing of points and polygons.
 */
public class InteractiveMap extends com.teamdev.jxmaps.swing.MapView {
    protected final MapApplication parent;
    protected final Page pageType;
    protected Map map;
    protected boolean loaded;

    public InteractiveMap(MapViewOptions options, MapApplication parent, Page pageType) {
        super(options);
        this.parent = parent;
        this.pageType = pageType;
        this.map = null;
        this.loaded = false;
    }

    LatLng getCenter(ArrayList<LatLng> points) {
        double minLat = 91;
        double maxLat = -91;
        double minLng = 181;
        double maxLng = -181;
        for (LatLng point : points) {
            if (point.getLat() > maxLat)
                maxLat = point.getLat();
            if (point.getLat() < minLat)
                minLat = point.getLat();
            if (point.getLng() > maxLng)
                maxLng = point.getLng();
            if (point.getLng() < minLng)
                minLng = point.getLng();
        }
        return new LatLng((maxLat + minLat) / 2, (maxLng + minLng) / 2);
    }

    void generatePolygon(LatLng[] polygonCoordinates) {
        Polygon placePolygon = new Polygon(this.map);
        placePolygon.setPath(polygonCoordinates);

        PolygonOptions options = new PolygonOptions();
        options.setFillColor("#FF0000");
        options.setFillOpacity(0.3);
        options.setStrokeColor("#FF0000");
        options.setStrokeOpacity(0.75);
        options.setStrokeWeight(2.0);
        placePolygon.setOptions(options);
    }

    protected LatLng getPlaceCenter(PlaceListModel detail) {
        ArrayList<LatLng> temp = new ArrayList<>();
        for (CoordinatesModel point: detail.getCoordinates())
            temp.add(new LatLng(point.x, point.y));
        temp.add(new LatLng(detail.getCoordinates().get(0).x, detail.getCoordinates().get(0).y));
        LatLng placePoints[] = new LatLng[temp.size()];
        placePoints = temp.toArray(placePoints);

        generatePolygon(placePoints);

        return getCenter(temp);
    }

    public boolean isLoaded() {
        return loaded;
    }
}
