package vutbr.fit.gui.view.map;

import com.teamdev.jxmaps.*;
import vutbr.fit.gui.model.CoordinatesModel;
import vutbr.fit.gui.model.EventListModel;
import vutbr.fit.gui.model.PlaceListModel;
import vutbr.fit.gui.view.MapApplication;
import vutbr.fit.gui.view.Page;

import java.util.ArrayList;

import static com.teamdev.jxmaps.GeocoderStatus.OK;

/**
 * UserEventMap is responsible for creating a map containing markers and corresponding info windows representing the
 * logged user's events.
 */
public class UserEventMap extends InteractiveMap {
    public UserEventMap(MapViewOptions options, MapApplication parent, Page pageType) {
        super(options, parent, pageType);

        setOnMapReadyHandler(new MapReadyHandler() {
            public void onMapReady(MapStatus status) {

                for (EventListModel model: parent.getController().getUserEvents()) {
                    parent.getController().loadEvent(model);
                    PlaceListModel place = new PlaceListModel();
                    place.setId(model.getPlaceId());
                    parent.getController().loadPlace(place);

                    LatLng center = drawPlacePolygon();
                    Marker marker = new Marker(map);
                    marker.setPosition(center);

                    InfoWindow window = new InfoWindow(map);
                    window.setContent("<b>"+ model.getTitle() + "</b><br />" + parent.getController().getLoadedPlace()
                            .getPlaceName());
                    marker.addEventListener("click", new MapMouseEvent() {
                        @Override
                        public void onEvent(MouseEvent mouseEvent) {
                            parent.getController().loadEvent(model);
                            PlaceListModel place = new PlaceListModel();
                            place.setId(model.getPlaceId());
                            parent.getController().loadPlace(place);

                            parent.updateFrame(Page.EDIT_EVENT);
                        }
                    });
                    window.open(map, marker);
                }

                if (status == MapStatus.MAP_STATUS_OK) {
                    loaded = true;
                    map = getMap();
                    map.setZoom(12.0);
                    GeocoderRequest request = new GeocoderRequest();
                    request.setAddress("Brno");

                    // set map center to Brno
                    getServices().getGeocoder().geocode(request, new GeocoderCallback(map) {
                        @Override
                        public void onComplete(GeocoderResult[] result, GeocoderStatus status) {
                            if (status != OK)
                                return;
                            try {
                                map.setCenter(result[0].getGeometry().getLocation());
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    });
                }
            }
        });
    }

    private LatLng drawPlacePolygon() {
        ArrayList<LatLng> placeCoordinates = getPlaceCoordinates();
        LatLng polygonCoordinates[] = new LatLng[placeCoordinates.size()];
        polygonCoordinates = placeCoordinates.toArray(polygonCoordinates);

        generatePolygon(polygonCoordinates);

        return getCenter(placeCoordinates);
    }

    private ArrayList<LatLng> getPlaceCoordinates() {
        ArrayList<LatLng> temp = new ArrayList<>();
        for (CoordinatesModel point: parent.getController().getLoadedPlace().getCoordinates()) {
            temp.add(new LatLng(point.x, point.y));
        }
        temp.add(new LatLng(parent.getController().getLoadedPlace().getCoordinates().get(0).x,
                parent.getController().getLoadedPlace().getCoordinates().get(0).y));
        return temp;
    }
}
