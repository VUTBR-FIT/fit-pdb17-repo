package vutbr.fit.gui.view.map;

import com.teamdev.jxmaps.*;
import vutbr.fit.gui.model.CoordinatesModel;
import vutbr.fit.gui.view.MapApplication;
import vutbr.fit.gui.view.Page;
import vutbr.fit.gui.view.menu.SearchMenu;

import static com.teamdev.jxmaps.GeocoderStatus.OK;

/**
 * SearchMap class is responsible for generating a map and, based on the user input, changing the search radius center
 * address.
 */
public class SearchMap extends InteractiveMap {
    private Marker startMarker;
    private LatLng startingPoint;

    public SearchMap(MapViewOptions options, MapApplication parent, Page pageType) {
        super(options, parent, pageType);
        this.startMarker = null;
        this.startingPoint = null;

        setOnMapReadyHandler(status -> {
            if (status != MapStatus.MAP_STATUS_OK)
                return;

            loaded = true;
            map = getMap();
            map.setZoom(12.0);
            GeocoderRequest request = new GeocoderRequest();
            request.setAddress("Brno");

            // set map center to Brno
            getServices().getGeocoder().geocode(request, new GeocoderCallback(map) {
                @Override
                public void onComplete(GeocoderResult[] result, GeocoderStatus status) {
                    if (status != OK)
                        return;
                    map.setCenter(result[0].getGeometry().getLocation());
                }
            });

            generateStartingAddressSetter();
        });
    }

    private void generateStartingAddressSetter() {
        this.map.addEventListener("click", new MapMouseEvent() {
            @Override
            public void onEvent(MouseEvent mouseEvent) {
                if (startMarker != null)
                    startMarker.remove();

                final Marker marker = new Marker(map);
                marker.setPosition(mouseEvent.latLng());
                map.setCenter(mouseEvent.latLng());
                startingPoint = mouseEvent.latLng();
                final InfoWindow window = new InfoWindow(map);

                startMarker = marker;

                final GeocoderRequest request = new GeocoderRequest();
                request.setLocation(mouseEvent.latLng());

                getServices().getGeocoder().geocode(request, new GeocoderCallback(map) {
                    @Override
                    public void onComplete(GeocoderResult[] geocoderResults, GeocoderStatus geocoderStatus) {
                        if (geocoderStatus != GeocoderStatus.OK || geocoderResults.length == 0)
                            return;
                        GeocoderResult result = geocoderResults[0];
                        String currentAddress = result.getFormattedAddress().length() > 0 ? result.getFormattedAddress()
                                : result.getGeometry().getLocation().toString();
                        SearchMenu menu = (SearchMenu) parent.getContentLayout().getMenu();
                        menu.setStartAddress(currentAddress);
                        window.setContent("<b>Starting address</b><br />" + currentAddress);
                        window.open(map, marker);
                    }
                });
            }
        });
    }
    public void setStartingAddress(final String address) {
        if (startMarker != null)
            startMarker.remove();
        final GeocoderRequest request = new GeocoderRequest();
        request.setAddress(address);

        getServices().getGeocoder().geocode(request, new GeocoderCallback(map) {
            @Override
            public void onComplete(GeocoderResult[] geocoderResults, GeocoderStatus geocoderStatus) {
                if (geocoderStatus != GeocoderStatus.OK || geocoderResults.length == 0)
                    return;
                GeocoderResult result = geocoderResults[0];
                LatLng coords = result.getGeometry().getLocation();

                Marker marker = new Marker(map);
                marker.setPosition(coords);
                InfoWindow window = new InfoWindow(map);
                window.setContent("<b>Starting address</b><br />" + address);
                window.open(map, marker);
                startMarker = marker;
                map.setCenter(coords);
                startingPoint = coords;
            }
        });

    }

    public CoordinatesModel getStartingPoint() {
        if (startingPoint != null)
            return new CoordinatesModel((float) this.startingPoint.getLat(), (float) this.startingPoint.getLng());
        return new CoordinatesModel();
    }
}
