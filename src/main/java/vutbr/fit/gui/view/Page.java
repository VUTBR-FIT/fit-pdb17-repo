package vutbr.fit.gui.view;

/**
 * Page enum represents the types of individual screens and is used to distinguish them thorough the app.
 */
public enum Page {
    SEARCH_PAGE, SEARCH_RESULTS,
    DETAIL_PLACE, DETAIL_EVENT,
    EDIT_PLACE, EDIT_EVENT,
    USER_PLACES, USER_EVENTS, USER_FAVOURITES
}
