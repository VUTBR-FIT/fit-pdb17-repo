package vutbr.fit.gui.view;

import com.teamdev.jxmaps.MapViewOptions;
import vutbr.fit.gui.model.ImageModel;
import vutbr.fit.gui.view.map.*;
import vutbr.fit.gui.view.menu.*;
import vutbr.fit.gui.view.misc.ImagePanel;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;

/**
 * ContentLayout class represents the layout of the content pane of the application.
 */
public class ContentLayout extends JPanel {
    private final Page pageType;
    private MapApplication parent;
    private SideMenu menu;
    private JPanel contentArea;
    private InteractiveMap map;
    private ArrayList<ImageModel> slideshowImages;
    private int slideshowIndex;

    ContentLayout(MapApplication parent, Page pageType) {
        this.parent = parent;
        this.pageType = pageType;
        this.setLayout(new BorderLayout());
        generateSideMenu(parent);
        this.add(this.menu, BorderLayout.WEST);
        generateContentArea(parent);
    }

    private void generateSideMenu(MapApplication parent) {
        switch (pageType) {
            case SEARCH_PAGE:
                this.menu = new SearchMenu(parent);
                break;
            case SEARCH_RESULTS:
                this.menu = new SearchResultMenu(parent);
                break;
            case DETAIL_PLACE:
                this.menu = new PlaceDetailMenu(parent, parent.getController().getLoadedPlace());
                break;
            case DETAIL_EVENT:
                this.menu = new EventDetailMenu(parent, parent.getController().getLoadedEvent());
                break;
            case EDIT_EVENT:
                this.menu = new EventEditMenu(parent, parent.getController().getLoadedEvent());
                break;
            case EDIT_PLACE:
                this.menu = new PlaceEditMenu(parent, parent.getController().getLoadedPlace());
                break;
            case USER_EVENTS:
                this.menu = new UserEventsMenu(parent);
                break;
            case USER_PLACES:
                this.menu = new UserPlacesMenu(parent);
                break;
            case USER_FAVOURITES:
                this.menu = new UserFavouriteMenu(parent);
                break;
        }
    }

    private void generateContentArea(MapApplication parent) {
        this.contentArea = new JPanel(new BorderLayout());
        if (pageType == Page.DETAIL_EVENT && parent.getController().getLoadedEvent().getImages().size() > 0)
            initSlideshow();
        else if (pageType == Page.DETAIL_PLACE && parent.getController().getLoadedPlace().getImages().size() > 0)
            initSlideshow();
        else
            initMap(pageType);
        JMenuBar contentOperation = new NavigationMenubar(parent, pageType);
        this.contentArea.add(contentOperation, BorderLayout.PAGE_START);
    }

    void initSlideshow() {
        if (pageType == Page.DETAIL_EVENT)
            slideshowImages = parent.getController().getLoadedEvent().getImages();
        else
            slideshowImages = parent.getController().getLoadedPlace().getImages();
        drawSlideshowImage();

    }

    private void drawSlideshowImage() {
        this.contentArea.removeAll();
        this.contentArea = new ImagePanel(parent, this.slideshowImages.get(slideshowIndex));
        this.add(this.contentArea, BorderLayout.CENTER);
    }

    public void nextPicture() {
        this.slideshowIndex = (++this.slideshowIndex) % this.slideshowImages.size();
        drawSlideshowImage();
    }

    public void previousPicture() {
        this.slideshowIndex = this.slideshowIndex - 1 < 0 ? this.slideshowIndex-- : this.slideshowImages.size() - 1;
        drawSlideshowImage();
    }

    public void initMap(Page pageType) {
        generateMap(pageType);
        this.add(this.contentArea, BorderLayout.CENTER);
    }

    private void generateMap(Page pageType) {
        MapViewOptions options = new MapViewOptions();
        options.importPlaces();
        switch (pageType) {
            case SEARCH_PAGE:
                this.map = new SearchMap(options, parent, pageType);
                break;
            case EDIT_PLACE:
                this.map = new PlaceEditMap(options, parent, pageType);
                break;
            case USER_EVENTS:
                this.map = new UserEventMap(options, parent, pageType);
                break;
            case USER_PLACES:
            case SEARCH_RESULTS:
            case USER_FAVOURITES:
                this.map = new InteractiveMarkerMap(options, parent, pageType);
                break;
            case DETAIL_PLACE:
            case DETAIL_EVENT:
            case EDIT_EVENT:
                this.map = new PlainMarkerMap(options, parent, pageType);
                break;
        }
        JPanel mapPanel = new JPanel(new BorderLayout());
        mapPanel.add(map, BorderLayout.CENTER);
        this.contentArea.add(mapPanel, BorderLayout.CENTER);
    }

    public InteractiveMap getMap() {
        return this.map;
    }

    public ImageModel getCurrentPhoto() {
        return this.slideshowImages.get(this.slideshowIndex);
    }

    public SideMenu getMenu() {
        return menu;
    }

    public void resetMap() {
        this.map = null;
    }
}
