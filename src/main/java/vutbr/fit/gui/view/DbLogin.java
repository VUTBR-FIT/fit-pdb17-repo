package vutbr.fit.gui.view;

import vutbr.fit.configurator.Configurator;
import vutbr.fit.db.DbConnection;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * DbLogin class is responsible for creating a database login window used to access the Gort server.
 */
public class DbLogin extends JFrame {
    public DbLogin(Configurator configurator) {
        this.setTitle("Gort Login");
        this.setSize(400, 100);
        this.setLocationRelativeTo(this);
        this.setResizable(false);
        this.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

        JPanel panel = new JPanel(new GridBagLayout());
        GridBagConstraints gbc = new GridBagConstraints();
        gbc.fill = GridBagConstraints.HORIZONTAL;

        gbc.gridx = gbc.gridy = 0;
        JLabel label = new JLabel("Username:  ");
        panel.add(label, gbc);

        gbc.gridx = 1;
        JTextField username = new JTextField(20);
        username.setText("xcerny63");
        panel.add(username, gbc);

        gbc.gridy = 1;
        gbc.gridx = 0;
        label = new JLabel("Password: ");
        panel.add(label, gbc);

        gbc.gridx = 1;
        JPasswordField passwd = new JPasswordField(20);
        passwd.setText("jha0bpe8");
        panel.add(passwd, gbc);

        JButton login = new JButton("Log in");
        login.addActionListener(e -> {
            configurator.setDbConfig("jdbc:oracle:thin:@gort.fit.vutbr.cz:1521:gort", username.getText(),
                    new String(passwd.getPassword()));
            configurator.setConnectionBuilder(new DbConnection());
            // Open GUI
            configurator.run();
            this.dispose();
        });
        gbc.gridy = 2;
        gbc.gridx = 0;
        gbc.gridwidth = 2;
        panel.add(login, gbc);

        this.setContentPane(panel);
    }
}
