package vutbr.fit.db;

import oracle.jdbc.pool.OracleDataSource;
import vutbr.fit.configurator.IDbConnection;

import java.sql.Connection;
import java.sql.SQLException;

/**
 * Class for manage connection to database
 */
public class DbConnection implements IDbConnection {
    private Connection connection = null;

    /**
     * Return active connection
     *
     * @return connection
     */
    public Connection getConnection() {
        return getConnection(null,null,null);
    }

    @Override
    public Connection getConnection(String host, String username, String password) {
        if (connection != null)
            return connection;
        OracleDataSource ods = null;
        try {
            // create a OracleDataSource instance
            ods = new OracleDataSource();

        } catch (SQLException sqlEx) {
            System.err.println("DataSourceException: " + sqlEx.getMessage());
            return null;
        }
        ods.setURL(host);
        ods.setUser(username);
        ods.setPassword(password);

        // connect to the database
        try{
            connection = ods.getConnection();
        }catch(SQLException ex){
            close();
            System.err.println("ConnectionException: " + ex.getMessage());
        }
        return connection;
    }

    /**
     * Close active connection
     */
    public void close() {
        try {
            if (connection != null)
                connection.close();
        } catch (SQLException e) {
            connection= null;
        }
    }
}
