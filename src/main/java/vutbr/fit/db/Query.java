package vutbr.fit.db;

import java.sql.*;

public class Query {
    protected Connection connection;
    private Statement statement = null;
    public Query(Connection connection) {
        super();
        this.connection = connection;
    }

    public Connection getConnection() {
        return connection;
    }

    /**
     * Method for create prepared statement object from string
     *
     * @param statement prepared SQL string
     * @return prepared statement object
     * @throws SQLException
     */
    public PreparedStatement prepare(String statement) throws SQLException {
        return getConnection().prepareStatement(statement);
    }

    /**
     * Method for create prepared statement object from string
     *
     * @param statement     prepared SQL string
     * @param add_code      auto-generated keys
     * @return prepared statement object
     * @throws SQLException
     */
    public PreparedStatement prepare(String statement, int add_code) throws SQLException {
        return getConnection().prepareStatement(statement, add_code);
    }

    /**
     * Method for create prepared statement object from string
     *
     * @param statement     prepared SQL string
     * @param generatedColumns      manually-generated columns
     * @return prepared statement object
     * @throws SQLException
     */
    public PreparedStatement prepare(String statement, String[] generatedColumns) throws SQLException {
        return getConnection().prepareStatement(statement, generatedColumns);
    }

    /**
     * Create empty statement
     * @return empty statement
     * @throws SQLException
     */
    public Statement getStatement() throws SQLException {
        return statement = getConnection().createStatement();
    }

    /**
     * Execute query
     * @param query
     * @return
     * @throws SQLException
     */
    public ResultSet execute(String query) throws SQLException {
        return statement.executeQuery(query);
    }
}
