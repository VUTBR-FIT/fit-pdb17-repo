package vutbr.fit.db.model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class UserModel extends Model {
    public static final String TABLE_NAME = "USER_DATA";
    public static final String COL_ID = "ID";
    public static final String COL_NAME = "NAME";
    public static final String COL_SURNAME = "SURNAME";
    public static final String COL_EMAIL = "EMAIL";
    public static final String COL_PASSWORD = "PASSWORD";

    public UserModel(Connection connection) {
        super(connection);
    }

    /**
     * User authentication by his email and password
     *
     * @param email
     * @param password
     * @return Logged in user
     */
    public ResultSet authenticate(String email, String password) {
        if(email.length() == 0 || password.length() == 0)
            return null;
        ResultSet user = getUserByEmail(email);
        try {
            if (user.next()) {
                if (user.getString(COL_PASSWORD).equals(password))
                    return user;
            }
        } catch (SQLException e){
            System.out.println(e);
        }
        return null;
    }

    public boolean registerUser(String name, String surname, String email, String password){
        if(email.length() == 0 || password.length() == 0)
            return false;
        ResultSet user_exists = getUserByEmail(email);
        try{
            if(user_exists.next()){
                return false;
            }

        }catch(SQLException e){
            return false;
        }
        try{
            PreparedStatement preparedStatement = getPrepareStatement("INSERT INTO " + getTableName() + " (" + COL_NAME + ", " + COL_SURNAME + ", " + COL_EMAIL + ", " + COL_PASSWORD + ") VALUES (?, ?, ?, ?)");
            preparedStatement.setString(1, name);
            preparedStatement.setString(2, surname);
            preparedStatement.setString(3, email);
            preparedStatement.setString(4, password);
            return preparedStatement.executeUpdate() == 1;
        }catch(SQLException e){
            return false;
        }
    }

    public ResultSet getUserById(int id){
        String query = getBasicSelect() + " WHERE " + COL_ID + " = ?";
        try {
            PreparedStatement preparedStatement = getPrepareStatement(query);
            preparedStatement.setInt(1, id);
            return preparedStatement.executeQuery();
        } catch (SQLException e) {
            return null;
        }
    }

    public ResultSet getUserByEmail(String email) {
        String query = getBasicSelect() + " WHERE " + COL_EMAIL + " = ?";
        try {
            PreparedStatement preparedStatement = getPrepareStatement(query);
            preparedStatement.setString(1, email);
            return preparedStatement.executeQuery();
        } catch (SQLException e) {
            return null;
        }
    }

    public ResultSet getUserPlaces(int user_id){
        String query = "SELECT p.* FROM " + PlaceModel.TABLE_NAME + " p LEFT JOIN PERMISSION_PLACE pp "
                + " ON p." + PlaceModel.COL_ID + " = pp.PLACE_ID "
                + " WHERE p.USER_ID = ? OR (pp.USER_ID = ? AND (C = 1 OR R = 1 OR U = 1))";
        try{
            PreparedStatement preparedStatement = getPrepareStatement(query);
            preparedStatement.setInt(1, user_id);
            preparedStatement.setInt(2, user_id);
            return preparedStatement.executeQuery();

        }catch(SQLException e){
            return null;
        }
    }

    public ResultSet getUserEvents(int user_id){
        String query = getBasicSelect("PERMISSION_ACTION")+" WHERE USER_ID = ? AND (R = 1 OR C = 1 OR U = 1 OR D = 1)";
        try{
            PreparedStatement preparedStatement = getPrepareStatement(query);
            preparedStatement.setInt(1, user_id);
            return preparedStatement.executeQuery();

        }catch(SQLException e){
            return null;
        }
    }

    private String getBasicSelect(String tableName){
        return "SELECT * FROM " + tableName;
    }

    protected String getBasicSelect() {
        return "SELECT * FROM " + getTableName();
    }

    @Override
    public String getTableName() { return TABLE_NAME; }
}
