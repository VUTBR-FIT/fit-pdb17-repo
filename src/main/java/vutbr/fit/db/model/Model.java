package vutbr.fit.db.model;

import vutbr.fit.db.Query;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.HashMap;

public abstract class Model {
    protected Connection connection;

    private HashMap<String, PreparedStatement> preparedStatement = new HashMap<>();

    public enum Names {
        USER,
        PLACE,
        EVENT,
        IMAGE
    }

    public Model(Connection connection) {
        this.connection = connection;
    }

    public Query createQuery() {
        return new Query(connection);
    }

    public PreparedStatement getPrepareStatement(String statement) throws SQLException {
        if (preparedStatement.containsKey(statement)) {
            return preparedStatement.get(statement);
        } else {
            PreparedStatement temp = createQuery().prepare(statement);
            preparedStatement.put(statement, temp);
            return temp;
        }
    }

    public PreparedStatement getPrepareStatement(String statement, int add_code) throws SQLException{
        if (preparedStatement.containsKey(statement)) {
            return preparedStatement.get(statement);
        } else {
            PreparedStatement temp = createQuery().prepare(statement, add_code);
            preparedStatement.put(statement, temp);
            return temp;
        }
    }

    public PreparedStatement getPrepareStatement(String statement, String[] generatedColumns) throws SQLException{
        if (preparedStatement.containsKey(statement)) {
            return preparedStatement.get(statement);
        } else {
            PreparedStatement temp = createQuery().prepare(statement, generatedColumns);
            preparedStatement.put(statement, temp);
            return temp;
        }
    }

    protected String getBasicSelect() {
        return "SELECT * FROM " + getTableName();
    }

    public abstract String getTableName();

}
