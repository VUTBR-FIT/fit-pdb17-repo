package vutbr.fit.db.model;

import vutbr.fit.gui.model.*;

import java.sql.*;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

public class EventModel extends Model {
    public static final String TABLE_NAME = "ACTION";
    public static final String COL_ID = "ID";
    public static final String COL_PLACE_ID = "PLACE_ID";
    public static final String COL_USER_ID = "USER_ID";
    public static final String COL_NAME = "TITLE";
    public static final String COL_DESCRIPTION = "DESCRIPTION";
    public static final String COL_BEGIN = "BEGIN";
    public static final String COL_END = "END";

    public EventModel(Connection connection) {
        super(connection);
    }

    public EventDetailModel loadEventDetail(int eventId){
        EventDetailModel event = new EventDetailModel();

        //get event data
        try{
            PreparedStatement pstmt_eventData = getPrepareStatement("SELECT * FROM ACTION WHERE " + COL_ID + " = ?");
            pstmt_eventData.setInt(1, eventId);
            ResultSet eventData = pstmt_eventData.executeQuery();

            if(!eventData.next()){
                return null;
            }
            event.setId(eventData.getInt(COL_ID));
            event.setTitle(eventData.getString(COL_NAME));
            event.setPlaceId(eventData.getInt(COL_PLACE_ID));
            event.setText(eventData.getString(COL_DESCRIPTION));

            DateFormat df = new SimpleDateFormat("dd.MM.yyyy");

            event.setStart(df.format(eventData.getDate("BEGIN")));
            event.setEnd(df.format(eventData.getDate("END")));
        }catch(SQLException e){

        }

        //get event comments
        try{
            ArrayList<CommentModel> resultComments = new ArrayList<>();

            PreparedStatement pstmt_commentsEvent = getPrepareStatement("SELECT * FROM ACTION_COMMENT WHERE ACTION_ID = ?");
            pstmt_commentsEvent.setInt(1, eventId);
            ResultSet eventComments = pstmt_commentsEvent.executeQuery();
            while(eventComments.next()){
                //get comment data
                PreparedStatement pstmt_commentData = getPrepareStatement("SELECT * FROM COMMENT WHERE ID = ?");
                pstmt_commentData.setInt(1, eventComments.getInt("COMMENT_ID"));
                ResultSet commentData = pstmt_commentData.executeQuery();
                if(commentData.next()){
                    ResultSet userData = new UserModel(connection).getUserById(commentData.getInt("USER_ID"));
                    userData.next();
                    CommentModel comment = new CommentModel(commentData.getInt("ID"), userData.getString("EMAIL"), commentData.getString("TITLE"), commentData.getString("CONTENT"));
                    resultComments.add(comment);
                }
            }
            event.setComments(resultComments);
        }catch(SQLException e){

        }
        //get place images
        // todo nacteni obrazku k mistu
        return event;
    }

    public ResultSet getEventData(int id){
        String query = getBasicSelect() + " WHERE " + COL_ID + " = ?";
        try{
            PreparedStatement preparedStatement = getPrepareStatement(query);
            preparedStatement.setInt(1, id);
            return preparedStatement.executeQuery();
        }catch(SQLException e){
            return null;
        }
    }

    public ArrayList<EventListModel> getEventsByPlace(int placeId) {
        ArrayList<EventListModel> result = new ArrayList<>();
        try{
            DateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");
            java.util.Date date = new java.util.Date();
            PreparedStatement statementComments = getPrepareStatement("SELECT * FROM " + TABLE_NAME + " WHERE " + COL_PLACE_ID + " = ? AND END < TO_DATE('" + dateFormat.format(date) + "', 'dd.MM.yyyy')");
            System.out.println("SELECT * FROM " + TABLE_NAME + " WHERE " + COL_PLACE_ID + " = ? AND END < TO_DATE('" + dateFormat.format(date) + "', 'dd.MM.yyyy')");
            statementComments.setInt(1, placeId);

            ResultSet events = statementComments.executeQuery();
            while(events.next()){
                EventDetailModel model = new EventDetailModel();
                model.setId(events.getInt(COL_ID));
                model.setStart(events.getDate(COL_BEGIN).toString());
                model.setEnd(events.getDate(COL_BEGIN).toString());
                model.setPlaceId(events.getInt(COL_PLACE_ID));
                model.setText(events.getString(COL_DESCRIPTION));
                model.setTitle(events.getString(COL_NAME));

                result.add(model);
            }
        }catch(SQLException e){
            System.err.println("get events " + e);
        }
        return result;
    }

    public boolean addComment(CommentModel comment, int actionId, int userId){
        int commentId = 0;
        try{
            PreparedStatement comment_data = getPrepareStatement("INSERT INTO COMMENT_DATA (USER_ID, TITLE, CONTENT, CREATE_AT) VALUES (?, ?, ?, ?)", Statement.RETURN_GENERATED_KEYS);
            comment_data.setInt(1, userId);
            comment_data.setString(2, comment.getTitle());
            comment_data.setString(3, comment.getMessageText());
            comment_data.setDate(4, new Date(new java.util.Date().getTime()));
            if(comment_data.executeUpdate() == 1){
                //get inserted comment id
                ResultSet rs = comment_data.getGeneratedKeys();
                oracle.sql.ROWID rid = (oracle.sql.ROWID) rs.getObject(1);

                commentId = rid.intValue();
            }else{
                return false;
            }
        }catch(SQLException e){
            return false;
        }
        if(commentId == 0)
            return false;
        try{
            PreparedStatement comment_place = getPrepareStatement("INSERT INTO COMMENT_ACTION (ACTION_ID, COMMENT_ID) VALUES (?, ?)");
            comment_place.setInt(1, actionId);
            comment_place.setInt(2, commentId);
            return comment_place.executeUpdate() == 1;
        }catch(SQLException e){
            return false;
        }
    }

    /**
     * Udate event in database
     *
     * @param id
     * @param title
     * @param text
     * @param start
     * @param end
     * @return
     */
    public boolean update(int id, String title, String text, String start, String end){
        SimpleDateFormat format = new SimpleDateFormat("dd.MM.yyyy");
        java.sql.Date sql_start = null;
        java.sql.Date sql_end = null;
        try{
            sql_start = new java.sql.Date(format.parse(start).getTime());
            sql_end = new java.sql.Date(format.parse(end).getTime());
        }catch(ParseException e){
            System.err.println("update event (date): " + e);
            return false;
        }

        try{
            PreparedStatement preparedStatement = getPrepareStatement(getBasicUpdate() + " WHERE " + COL_ID + " = ?");
            preparedStatement.setString(1, title);
            preparedStatement.setString(2, text);
            preparedStatement.setDate(3, sql_start);
            preparedStatement.setDate(4, sql_end);
            preparedStatement.setInt(5, id);

            return preparedStatement.executeUpdate() == 1;
        }catch(SQLException e){
            System.err.println("update event: " + e);
            return false;
        }
    }

    public boolean create(String title, String text, String start, String end, int placeId, int userId) {
        try{
            PreparedStatement preparedStatement = getPrepareStatement(getBasicInsert(start, end));
            preparedStatement.setInt(1, placeId);
            preparedStatement.setString(2, title);
            preparedStatement.setString(3, text);
            preparedStatement.setInt(4, userId);

            return preparedStatement.executeUpdate() == 1;
        }catch(SQLException e){
            System.err.println("create event: " + e);
            return false;
        }
    }

    public void delete(int eventId){
        try{
            PreparedStatement pstmt = getPrepareStatement("DELETE FROM ACTION WHERE ID = ?");
            pstmt.setInt(1, eventId);
            pstmt.executeUpdate();
        }catch(SQLException e){

        }
    }

    private String getBasicInsert(String start, String end){
        return "INSERT INTO " + getTableName() + " (" + COL_PLACE_ID + ", " + COL_NAME + ", " + COL_DESCRIPTION + ", " + COL_BEGIN + ", " + COL_END + ", " + COL_USER_ID + ") VALUES (?, ?, ?, TO_DATE('"+start+"', 'dd.MM.yyyy'), TO_DATE('"+end+"', 'dd.MM.yyyy'), ?)";
    }

    private String getBasicUpdate(){
        return "UPDATE " + getTableName() + " SET " + COL_NAME + " = ?, " + COL_DESCRIPTION + " = ?, " + COL_BEGIN + " = ?, " + COL_END + " = ?";
    }

    @Override
    public String getTableName() {
        return TABLE_NAME;
    }
}
