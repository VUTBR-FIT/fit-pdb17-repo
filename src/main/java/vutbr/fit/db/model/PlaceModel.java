package vutbr.fit.db.model;

import oracle.spatial.geometry.JGeometry;
import vutbr.fit.gui.model.*;

import java.sql.*;
import java.util.ArrayList;

public class PlaceModel extends Model {
    public static final String TABLE_NAME = "PLACE";
    public static final String COL_ID = "ID";
    public static final String COL_NAME = "TITLE";
    public static final String COL_LOCATION = "LOCATION";
    public static final String COL_ADDRESS = "ADDRESS";
    public static final String COL_DESCRIPTION = "DESCRIPTION";
    public static final String COL_USER_ID = "USER_ID";
    /**
     * circa one kilometer in Czech Republic
     */
    public static final Integer KILOMETER = 2250000;

    public PlaceModel(Connection connection) {
        super(connection);
    }

    public ResultSet getPlaceData(int id){
        String query = getBasicSelect() + " WHERE " + COL_ID + " = ?";
        try{
            PreparedStatement preparedStatement = getPrepareStatement(query);
            preparedStatement.setInt(1, id);
            return preparedStatement.executeQuery();
        }catch(SQLException e){
            return null;
        }
    }

    /**
     * Ceck if place is favorite
     *
     * @param userId
     * @param placeId
     * @return
     */
    public boolean isFavoritePlace(int userId, int placeId) {
        String query = "SELECT * FROM PLACE_FAVORITE WHERE PLACE_ID = ? AND USER_ID = ?";
        try {
            PreparedStatement statement = getPrepareStatement(query);
            statement.setInt(1, placeId);
            statement.setInt(2, userId);
            ResultSet resultSet = statement.executeQuery();
            return resultSet.next();
        } catch (SQLException e) {
            System.err.println("is favorite: " + e);
        }

        return false;
    }

    /**
     * Select favorite places
     *
     * @param userId
     * @return favorite places
     */
    public ArrayList<PlaceListModel> selectFavoritePlaces(int userId) {
        String query = "SELECT p.* FROM " + getTableName() + " p JOIN PLACE_FAVORITE pf ON p." + COL_ID + " = pf.PLACE_ID WHERE pf.USER_ID = ?";
        ArrayList<PlaceListModel> places = new ArrayList<>();
        try {
            PreparedStatement statement = getPrepareStatement(query);
            statement.setInt(1, userId);
            ResultSet results = statement.executeQuery();
            while (results.next()) {
                PlaceListModel place = new PlaceListModel(results.getInt(COL_ID), results.getString(COL_NAME));
                place.setAddress(results.getString(COL_ADDRESS));
                place.setCoordinates(convertLocationToCoords(results.getObject(COL_LOCATION)));
                places.add(place);
            }
        } catch (SQLException e) {
            System.err.println(e);
        } finally {
            return places;
        }
    }

    /**
     * Remove place from favorite list of user
     * @param placeId
     * @param userId
     * @return
     */
    public boolean removeFavorite(int placeId, int userId) {
        String query = "DELETE FROM PLACE_FAVORITE WHERE PLACE_ID = ? AND USER_ID = ?";
        try {
            PreparedStatement statement = getPrepareStatement(query);
            statement.setInt(1, placeId);
            statement.setInt(2, userId);
            return statement.execute();
        } catch (SQLException e ) {
            System.err.println("Remove favorite: " + e);
            return false;
        }
    }

    /**
     * Add place to favorite list of user
     *
     * @param placeId
     * @param userId
     * @return
     */
    public boolean addFavorite(int placeId, int userId) {
        String query = "INSERT INTO PLACE_FAVORITE (USER_ID, PLACE_ID) VALUES (?, ?)";
        try {
            PreparedStatement statement = getPrepareStatement(query);
            statement.setInt(1, userId);
            statement.setInt(2, placeId);
            return 0 != statement.executeUpdate();
        } catch (SQLException e ) {
            System.err.println("Add favorite: " + e);
            return false;
        }
    }

    /**
     * Select a location by name and location. We can selected places sort by distance from point.
     * @param name   Full name or part of name
     * @param point  Middle
     * @param sort   Enable sorting
     * @return
     */
    public ArrayList<PlaceListModel> getPlacesByNameAndPosition(String name, CoordinatesModel point, int radius, boolean sort) {
        ArrayList<PlaceListModel> places = new ArrayList<>();
        try {

            Integer distance = KILOMETER * radius;
            String points = (getDBValue(point.x) + distance) + ", " + getDBValue(point.y) + ", " + getDBValue(point.x) + ", " + (getDBValue(point.y) + distance) + ", " + (getDBValue(point.x) - distance) + ", " + getDBValue(point.y);

            String query;
            if (sort)
                query = "SELECT t.*, SDO_GEOM.SDO_DISTANCE(LOCATION, SDO_GEOMETRY(2001, NULL, SDO_POINT_TYPE("
                    + getDBValue(point.x) + ", " + getDBValue(point.y) + ", NULL), NULL, NULL), 1) AS DIST FROM "
                    + TABLE_NAME + " t WHERE " + COL_NAME + " LIKE ? AND SDO_RELATE(LOCATION, "
                        + getSDOTemplate(2, points) + ", 'mask=ANYINTERACT') = 'TRUE' ORDER BY DIST";
            else
                query = getBasicSelect() + " WHERE " + COL_NAME + " LIKE ? AND SDO_RELATE(LOCATION, "
                    + getSDOTemplate(2, points) + ", 'mask=ANYINTERACT') = 'TRUE'";

            PreparedStatement stmt = getPrepareStatement(query);
            stmt.setString(1, "%" + name + "%");

            ResultSet results = stmt.executeQuery();

            PlaceListModel place;
            while (results.next()) {
                if (results.getObject(COL_LOCATION) == null)
                    continue;
                place = new PlaceListModel(results.getInt(COL_ID), results.getString(COL_NAME));
                place.setAddress(results.getString(COL_ADDRESS));
                place.setCoordinates(convertLocationToCoords(results.getObject(COL_LOCATION)));
                places.add(place);
            }
        } catch (SQLException e) {
            System.err.println(e);
        }
        return places;
    }

    /**
     * Select a location by name. We can selected places sort by distance from point.
     * @param name   Full name or part of name
     * @param point  Middle
     * @param sort   Enable sorting
     * @return
     */
    public ArrayList<PlaceListModel> getPlacesByName(String name, CoordinatesModel point, boolean sort){
        ArrayList<PlaceListModel> places = new ArrayList<>();
        try{
            String query;
System.out.println(point);
            if (sort)
                query = "SELECT t.*, SDO_GEOM.SDO_DISTANCE(LOCATION, SDO_GEOMETRY(2001, NULL, SDO_POINT_TYPE("
                        + getDBValue(point.x) + ", " + getDBValue(point.y) + ", NULL), NULL, NULL), 1) AS DIST FROM "
                        + TABLE_NAME + " t WHERE " + COL_NAME + " LIKE ? ORDER BY DIST";
            else
                query = "SELECT * FROM " + TABLE_NAME + " WHERE " + COL_NAME + " LIKE ?";

            PreparedStatement stmt = getPrepareStatement(query);
            stmt.setString(1, "%" + name + "%");

            ResultSet results = stmt.executeQuery();

            PlaceListModel place;
            while(results.next()){
                if (results.getObject(COL_LOCATION) == null)
                    continue;
                place = new PlaceListModel(results.getInt(COL_ID), results.getString(COL_NAME));
                place.setAddress(results.getString(COL_ADDRESS));
                place.setCoordinates(convertLocationToCoords(results.getObject(COL_LOCATION)));
                places.add(place);
            }
        } catch(SQLException e){
            System.err.println(e);
        }
        return places;
    }

    public PlaceDetailModel loadPlaceDetail(int placeId){
        PlaceDetailModel place = new PlaceDetailModel();

        //get place data
        try{
            PreparedStatement pstmt_placeData = getPrepareStatement(getBasicSelect() + " WHERE " + COL_ID + " = ?");
            pstmt_placeData.setInt(1, placeId);
            ResultSet placeData = pstmt_placeData.executeQuery();

            if(!placeData.next()){
                return place;
            }
            place.setId(placeData.getInt(COL_ID));
            place.setPlaceName(placeData.getString(COL_NAME));
            place.setDescription(placeData.getString(COL_DESCRIPTION));
            place.setCoordinates(convertLocationToCoords(placeData.getObject(COL_LOCATION)));

        }catch(SQLException e){

        }

        //get place comments
        try{
            ArrayList<CommentModel> resultComments = new ArrayList<>();
            PreparedStatement statementComments = getPrepareStatement("SELECT * FROM PLACE_COMMENT WHERE PLACE_ID = ?");
            statementComments.setInt(1, placeId);
            ResultSet placeComments = statementComments.executeQuery();
            while(placeComments.next()){
                //get comment data
                ResultSet userData = new UserModel(connection).getUserById(placeComments.getInt("USER_ID"));
                userData.next();
                CommentModel comment = new CommentModel(placeComments.getInt("ID"), userData.getString(UserModel.COL_EMAIL), placeComments.getString("TITLE"), placeComments.getString("CONTENT"));
                resultComments.add(comment);
            }
            place.setComments(resultComments);
        }catch(SQLException e){

        }
        return place;/*
        //get place images
        // todo nacteni obrazku k mistu
        //get place favorite users
        try{
            ArrayList<FavoriteModel> resultFavorites = new ArrayList<>();

            PreparedStatement pstmt_favoriteUsers = getPrepareStatement("SELECT * FROM PLACE_FAVORITE WHERE PLACE_ID = ?");
            pstmt_favoriteUsers.setInt(1, placeId);
            ResultSet favoriteUsers = pstmt_favoriteUsers.executeQuery();
            while(favoriteUsers.next()){
                ResultSet userData = new UserModel(connection).getUserById(favoriteUsers.getInt("USER_ID"));
                userData.next();
                FavoriteModel favorite = new FavoriteModel(userData.getInt("ID"), userData.getString("EMAIL"));
                favorite.setId(favoriteUsers.getInt("ID"));
                resultFavorites.add(favorite);
            }
            place.setFavorites(resultFavorites);
        }catch(SQLException e){

        }
        return place;*/
    }

    /**
     * Update row by ID
     *
     * @param id
     * @param name      new name of place
     * @param address   new address
     * @param desc      new description
     * @param coords    new points
     * @return success of operation
     */
    public boolean update(int id, String name, String address, String desc, ArrayList<CoordinatesModel> coords){
        try {
            String query = "UPDATE " + TABLE_NAME + " SET "
                    + COL_NAME + " = ?, "
                    + COL_LOCATION + " = " + getSDO(coords) + ", "
                    + COL_ADDRESS + " = ?, "
                    + COL_DESCRIPTION + " = ? WHERE "
                    + COL_ID + " = ? ";
System.out.println(query);
            PreparedStatement statement = createQuery().prepare(query);

            statement.setString(1, name);
            statement.setString(2, address);
            statement.setString(3, desc);
            statement.setInt(4, id);
            statement.executeUpdate();
            return true;
        } catch (Exception e) {
            System.err.println("update place " + e);
            return false;
        }
    }

    /**
     * Create new place
     *
     * @param name      Name of place
     * @param address   Address as string
     * @param desc      Short description of place
     * @param coords    Points
     * @param userId    Created by user with userId
     */
    public void create(String name, String address, String desc, ArrayList<CoordinatesModel> coords, int userId) {
        try {
            String query = "INSERT INTO " + TABLE_NAME + " ("
                    + COL_NAME + ", "
                    + COL_LOCATION + ", "
                    + COL_ADDRESS + ", "
                    + COL_DESCRIPTION + ", "
                    + COL_USER_ID + ") VALUES (?, " + getSDO(coords) + ", ?, ?, ? )";

            PreparedStatement statement = createQuery().prepare(query);
System.out.println(query);
            statement.setString(1, name);
            statement.setString(2, address);
            statement.setString(3, desc);
            statement.setInt(4, userId);
            statement.executeUpdate();
        } catch (Exception e) {
            System.err.println("create place " + e);
        }
    }

    /**
     * Convert list of CoordinatesModel to SDO_GEOMETRY
     *
     * @param coord points
     * @return
     */
    private String getSDO(ArrayList<CoordinatesModel> coord) {
        String sPoints = "";
        for (CoordinatesModel model: coord) {
            sPoints += "," + getDBValue(model.x) + ", " + getDBValue(model.y);
        }
        sPoints += "," + getDBValue(coord.get(0).x) + ", " + getDBValue(coord.get(0).y);
        switch (coord.size()) {
            case 1:
                return getSDOTemplate(1, getDBValue(coord.get(0).x) + ", " + getDBValue(coord.get(0).y));
            default:
                return getSDOTemplate(3, sPoints.substring(1));
        }
    }

    /**
     * Get template of SDO_GEOMETRY
     *
     * @param type
     * @param values inserted values
     * @return
     */
    private String getSDOTemplate(int type, String values) {
        switch (type) {
            case 1://point
                return "SDO_GEOMETRY(2001, NULL, SDO_POINT_TYPE(" + values + ", NULL), NULL, NULL)";
            case 2://circle
                return "SDO_GEOMETRY(2003, NULL, NULL, SDO_ELEM_INFO_ARRAY(1, 2003, 4), SDO_ORDINATE_ARRAY(" + values + "))";
            case 3://polygon
                return "SDO_GEOMETRY(2003, NULL, NULL, SDO_ELEM_INFO_ARRAY(1, 1003, 1), SDO_ORDINATE_ARRAY(" + values + "))";
            default:
                return "";
        }
    }

    public void delete(int placeId){
        try{
            PreparedStatement pstmt = getPrepareStatement("DELETE FROM PLACE WHERE ID = ?");
            pstmt.setInt(1, placeId);
            pstmt.executeUpdate();
        }catch(SQLException e){

        }
    }

    public boolean addComment(CommentModel comment, int placeId, int userId){
        int commentId = 0;
        try{
            PreparedStatement comment_data = getPrepareStatement("INSERT INTO COMMENT_DATA (USER_ID, TITLE, CONTENT, CREATE_AT) VALUES (?, ?, ?, ?)", Statement.RETURN_GENERATED_KEYS);
            comment_data.setInt(1, userId);
            comment_data.setString(2, comment.getTitle());
            comment_data.setString(3, comment.getMessageText());
            comment_data.setDate(4, new Date(new java.util.Date().getTime()));
            if(comment_data.executeUpdate() == 1){
                //get inserted comment id
                ResultSet rs = comment_data.getGeneratedKeys();
                oracle.sql.ROWID rid = (oracle.sql.ROWID) rs.getObject(1);

                commentId = rid.intValue();
            }else{
                return false;
            }
        }catch(SQLException e){
            return false;
        }
        try{
            PreparedStatement comment_place = getPrepareStatement("INSERT INTO COMMENT_PLACE (PLACE_ID, COMMENT_ID) VALUES (?, ?)");
            comment_place.setInt(1, placeId);
            comment_place.setInt(2, commentId);
            return comment_place.executeUpdate() == 1;
        }catch(SQLException e){
            return false;
        }
    }

    @Override
    public String getTableName() { return TABLE_NAME; }

    public double[] coordinatesToDouble(CoordinatesModel model){
        return new double[] {
                getDBValue(model.x),
                getDBValue(model.y)
        };
    }

    /**
     * Convert object to list of CoordinatesModel
     *
     * @param source object from DB query
     * @return
     */
    public static ArrayList<CoordinatesModel> convertLocationToCoords(Object source) {
        ArrayList<CoordinatesModel> result = new ArrayList<>();
        try {
            JGeometry geo = JGeometry.loadJS((Struct) source);
            double[] point;
            switch (geo.getNumPoints()) {
                case 1:
                    point = geo.getPoint();
                    result.add(new CoordinatesModel((float) getRealValue(point[0]), (float) getRealValue(point[1])));
                    break;
                case 2:

                    point = geo.getFirstPoint();
                    result.add(new CoordinatesModel((float) getRealValue(point[0]), (float) getRealValue(point[1])));

                    point = geo.getLastPoint();
                    result.add(new CoordinatesModel((float) getRealValue(point[0]), (float) getRealValue(point[1])));
                    break;
                default:
                    JGeometry[] te = geo.getElements();
                    point = geo.getOrdinatesArray();
                    int i = 0;
                    while (i < geo.getNumPoints()) {
                        result.add(new CoordinatesModel((float) getRealValue(point[i*2]), (float) getRealValue(point[i*2+1])));
                        i++;
                    }
                    break;
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return result;
    }

    /**
     * Convert real value of position to DB representation
     *
     * @param i
     * @return i * 10^9
     */
    public static double getDBValue(double i) {
        return i * 1000000000;
    }

    /**
     * Convert DB representation to real value of position
     * @param i
     * @return
     */
    public static double getRealValue(double i) {
        return i / 1000000000;
    }
}
