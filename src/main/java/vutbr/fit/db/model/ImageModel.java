package vutbr.fit.db.model;

import javax.imageio.ImageIO;
import javax.imageio.stream.ImageOutputStream;
import javax.swing.plaf.nimbus.State;

import oracle.jdbc.OraclePreparedStatement;
import oracle.jdbc.OracleResultSet;
import oracle.ord.im.*;
import oracle.sqlj.runtime.Oracle;
import vutbr.fit.configurator.Configurator;
import vutbr.fit.configurator.IDbConnection;

import java.awt.image.BufferedImage;
import java.awt.image.DataBufferByte;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.sql.*;

public class ImageModel extends Model {
    public static final String PLACE_TABLE_NAME = "PLACE_PHOTO";
    public static final String EVENT_TABLE_NAME = "ACTION_PHOTO";

    public static final String COL_EVENT_ID = "ACTION_ID";
    public static final String COL_PLACE_ID = "PLACE_ID";

    public static final String COL_ID = "ID";
    public static final String COL_USER_ID = "USER_ID";

    public static final String COL_INFO_SI = "INFO_SI";
    public static final String COL_INFO_AC = "INFO_AC";
    public static final String COL_INFO_CH = "INFO_CH";
    public static final String COL_INFO_PC = "INFO_PC";
    public static final String COL_INFO_TX = "INFO_TX";

    public static final String COL_CONTENT = "CONTENT";


    public ImageModel(Connection connection) {
        super(connection);
    }

    public boolean add(int userId, int placeId, BufferedImage image, Configurator configurator){
        Connection conn = configurator.getConnection();
        try{

            conn.setAutoCommit(false);

            String[] generatedColumn = {COL_ID};
            Statement pstmtInit = conn.createStatement();
            int rowsAffected = pstmtInit.executeUpdate("INSERT INTO PLACE_PHOTO ("
                    + COL_USER_ID + ", "
                    + COL_PLACE_ID + ", "
                    + COL_CONTENT
                    + ") VALUES (" + userId + ", " + placeId + ", ordsys.ordimage.init())", generatedColumn);

            int imgId = 0;
            if(rowsAffected == 1){
                //get initialized image id
                ResultSet rs = pstmtInit.getGeneratedKeys();
                rs.next();
                imgId = rs.getInt(1);
            }else{
                System.err.println("Add image init statement error");
                conn.setAutoCommit(true);
                return false;
            }
            pstmtInit.close();
            System.out.println("Add image init statement finished");

            System.out.println("Add image init statement 2 started");

            Statement stmtInit2 = conn.createStatement();
            OracleResultSet rs = (OracleResultSet) stmtInit2.executeQuery("SELECT " + COL_CONTENT + " FROM PLACE_PHOTO WHERE " + COL_ID + " = " + imgId + " FOR UPDATE");
            rs.next();
            OrdImage ordImage = (OrdImage) rs.getORAData(COL_CONTENT, OrdImage.getORADataFactory());
            rs.close();
            stmtInit2.close();
            System.out.println("Add image init statement 2 finished");

            System.out.println("Add image insert content statement started");
            byte[] imageInByte = ((DataBufferByte)(image).getRaster().getDataBuffer()).getData();
            System.out.println(ordImage.loadDataFromByteArray(imageInByte));
            ordImage.setProperties();
            System.out.println("test1");
            OraclePreparedStatement pstmtSaveContent = (OraclePreparedStatement) conn.prepareStatement("UPDATE PLACE_PHOTO SET "
                    + COL_CONTENT + " = ? "
                    + "WHERE " + COL_ID + " = ?");
            System.out.println("test2");
            pstmtSaveContent.setORAData(1, ordImage);
            pstmtSaveContent.setInt(2, imgId);
            System.out.println("test3");
            if(pstmtSaveContent.executeUpdate() != 1){
                System.err.println("Add image insert content statement error");
                conn.setAutoCommit(true);
                return false;
            }
            pstmtSaveContent.close();
            System.out.println("Add image insert content statement finished");

            System.out.println("Add image insert SI_StillImage info statement started");
            Statement stmt = conn.createStatement();
            rowsAffected = stmt.executeUpdate("UPDATE PLACE_PHOTO SET "
                    + "PLACE_PHOTO." + COL_INFO_SI + "=SI_StillImage(PLACE_PHOTO." + COL_CONTENT + ".getContent()) "
                    + "WHERE " + COL_ID + " = " + imgId);
            if(rowsAffected != 1){
                System.err.println("Add image insert SI_StillImage info statement error");
                conn.setAutoCommit(true);
                return false;
            }
            System.out.println("Add image insert SI_StillImage statement finished");

            System.out.println("Add image insert additional info statement started");
            rowsAffected = stmt.executeUpdate("UPDATE PLACE_PHOTO SET "
                    + "PLACE_PHOTO." + COL_INFO_AC + "=SI_AverageColor(PLACE_PHOTO.INFO)"
                    + "PLACE_PHOTO." + COL_INFO_CH + "=SI_ColorHistogram(PLACE_PHOTO.INFO)"
                    + "PLACE_PHOTO." + COL_INFO_PC + "=SI_PositionalColor(PLACE_PHOTO.INFO)"
                    + "PLACE_PHOTO." + COL_INFO_TX + "=SI_Texture(PLACE_PHOTO.INFO)"
                    + "WHERE " + COL_ID + " = "+ imgId
            );
            if(rowsAffected != 1){
                System.err.println("Add image insert additional info statement error");
                conn.setAutoCommit(true);
                return false;
            }
            conn.commit();
            System.out.println("Add image additional info statement finished");
            conn.close();
        }catch(SQLException | IOException e){
            System.err.println(e);
        }
        try{
            conn.setAutoCommit(true);
        }catch(SQLException e){

        }
        return true;
    }

    /*public BufferedImage read(int imgId){
        BufferedImage ret = new BufferedImage();
        return ret;
    }*/

    public boolean delete(){
        return true;
    }

    @Override
    public String getTableName() {
        return "";
    }
}
