package vutbr.fit.pdb17;

import vutbr.fit.configurator.Configurator;
import vutbr.fit.db.DbConnection;
import vutbr.fit.gui.view.DbLogin;

public class App 
{
    public static void main( String[] args )
    {
        Configurator configurator = new Configurator();
        DbLogin loginScreen = new DbLogin(configurator);
        loginScreen.setVisible(true);
    }
}
