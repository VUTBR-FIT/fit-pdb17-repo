package vutbr.fit.configurator;

import java.sql.Connection;

public interface IDbConnection {
    /**
     * Create new connection to database
     *
     * @param host
     * @param username
     * @param password
     * @return Connection New connection
     */
    public Connection getConnection(String host, String username, String password);

}
