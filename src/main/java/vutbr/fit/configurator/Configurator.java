package vutbr.fit.configurator;

import vutbr.fit.db.model.*;
import vutbr.fit.gui.view.MapApplication;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

/**
 * Class to configure a database connection
 */
public class Configurator {

    private Map<String, String> dbConfig = new HashMap<String, String>();
    /** Builder of database connection */
    private IDbConnection connectionBuilder = null;
    /** Active connection to database */
    private Connection connection = null;

    private HashMap<Model.Names, Model>  models = new HashMap<>();

    public Configurator() {
        dbConfig.put("host", "");
        dbConfig.put("user", "");
        dbConfig.put("pass", "");
    }

    /**
     * Create new connection
     *
     * @return new connection
     */
    public Connection createConnection() {
        return connectionBuilder.getConnection(dbConfig.get("host"), dbConfig.get("user"), dbConfig.get("pass"));
    }

    /**
     * Returns connection to database. Creation of connection is lazy.
     *
     * @return Connection
     */
    public Connection getConnection() {
        if (connection == null) {
            connection =createConnection();
        }
        return connection;
    }

    /**
     * Create and return DB model
     *
     * @param model Type of DB model
     * @return
     */
    public Model getModel(Model.Names model) {
        try {
            if (getConnection().isClosed()) {
                models.clear();
                connection = null;
            }
        } catch (SQLException e){
            System.err.println("Connection: " + e);
        }
        if (models.get(model) != null)
            return models.get(model);
        switch (model) {
            case USER:
                models.put(model, new UserModel(getConnection()));
                break;
            case PLACE:
                models.put(model, new PlaceModel(getConnection()));
                break;
            case EVENT:
                models.put(model, new EventModel(getConnection()));
                break;
            case IMAGE:
                models.put(model, new ImageModel(getConnection()));
                break;
            default:
        }
        return models.get(model);
    }

    /**
     * Set builder(constructor) for database connection
     *
     * @param builder
     */
    public void setConnectionBuilder(IDbConnection builder) {
        connectionBuilder = builder;
    }

    /**
     * Configuring connection to the database.
     *
     * @param host
     * @param user
     * @param password
     */
    public void setDbConfig(String host, String user, String password) {
        dbConfig.put("host", host);
        dbConfig.put("user", user);
        dbConfig.put("pass", password);
    }

    /**
     * Run GUI
     */
    public void run() {
        MapApplication app = new MapApplication(this);
        app.setVisible(true);
    }
}
