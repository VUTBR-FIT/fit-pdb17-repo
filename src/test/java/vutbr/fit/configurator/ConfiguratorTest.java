package vutbr.fit.configurator;

import junit.framework.TestCase;
import org.junit.Rule;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import java.sql.Connection;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class ConfiguratorTest extends TestCase {

    @Rule public MockitoRule mockitoRule = MockitoJUnit.rule();


    public void testGetConnection() {
        IDbConnection mockBuilder = mock(IDbConnection.class);
        Connection mockConnection = mock(Connection.class);
        when(mockBuilder.getConnection("", "", "")).thenReturn(mockConnection);

        Configurator config = new Configurator();
        config.setConnectionBuilder(mockBuilder);
        config.setDbConfig("", "", "");
        assertTrue(config.getConnection() instanceof Connection);
    }

    public void testSetConnectionBuilder() {
        Configurator config = new Configurator();
        config.setConnectionBuilder(mock(IDbConnection.class));
        assertTrue(true);
    }

    public void testSetDbConfig() {
        Configurator config = new Configurator();
        config.setDbConfig("", "", "");
        assertTrue(true);
    }

    public void runGui() {
        Configurator config = new Configurator();
        config.run();
        assertTrue(true);
    }

    public void getConnectionSimple() {
        Configurator config = new Configurator();
        config.setDbConfig("jdbc:oracle:thin:@gort.vutbr.fit.vutbr.cz:1521:gort", "xberan34", "u180enyi");
        assertTrue(config.getConnection() instanceof Connection);
    }

    public void getConnectionExeption() throws IllegalStateException {
        Configurator config = new Configurator();
        config.setDbConfig("", "", "");
        assertTrue(config.getConnection() instanceof Connection);
    }
}