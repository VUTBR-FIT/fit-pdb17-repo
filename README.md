# PDB - 2017

[![pipeline status](https://gitlab.com/VUTBR-FIT/fit-pdb17-repo/badges/develop/pipeline.svg)](https://gitlab.com/VUTBR-FIT/fit-pdb17-repo/commits/develop)

## Members
- xcerny63 Lukáš Černý
- xberan34 Martin Beran
- xklobu01 Dominika Klobučníková

## Install
1. ```$ make install```
2. run 
    1. for windows make ```$ run-win```
    2. for linux 32bit ```$ make run``` 
    2. for linux 64bit ```$ make run-linux64``` 
    
## Run on windows
 ```
 $ java  -classpath libs\jxmaps-1.3.1.jar;libs\license.jar;libs\jxmaps-win.jar;target\fit-1.0-SNAPSHOT.jar
 ```
 
## Requirements
* **java 1.8.***

## USERS (login:password)
- 'xcerny63@email.cz:heslo
- xklobu01@email.cz:heslo
- xberan34@email.cz:heslo
- fialka@email.cz:heslo